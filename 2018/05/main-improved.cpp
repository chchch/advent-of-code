#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>

// use a stack
size_t a(const std::string& data)
{
    std::string result{};
    for (const char c: data) {
        if (result.empty()) {
            result.push_back(c);
        } else {
            char last = result.back();
            if (std::abs(last - c) == std::abs('a' - 'A')) {
                result.pop_back();
            } else {
                result.push_back(c);
            }
        }
    }
    return result.size();
}

size_t b(const std::string& data)
{
    size_t min{data.size()};
    std::string abc{"abcdefghijklmnopqrstuvwxyz"};
    for (char c: abc) {
        auto one_less{data};
        one_less.erase(std::remove_if(
                one_less.begin(),
                one_less.end(),
                [c](const unsigned char cc){ return std::tolower(cc) == c; }), 
            one_less.end());
        min = std::min(min, a(one_less));
    }
    return min;
}

int main()
{
    std::string data;
    getline(std::cin, data);
    std::cout << a(data) << std::endl;
    std::cout << b(data) << std::endl;
}
