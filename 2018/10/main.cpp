#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <algorithm>
#include <limits>

const bool test{false};

struct Coordinate {
    int x{0}, y{0};
    Coordinate& operator+=(const Coordinate& rhs) {
        x = x + rhs.x;
        y = y + rhs.y;
        return *this;
    }
    Coordinate& operator-=(const Coordinate& rhs) {
        x = x - rhs.x;
        y = y - rhs.y;
        return *this;
    }
};

Coordinate operator+(Coordinate lhs, const Coordinate& rhs)
{
    lhs += rhs;
    return lhs;
}
Coordinate operator-(Coordinate lhs, const Coordinate& rhs)
{
    lhs -= rhs;
    return lhs;
}

std::ostream& operator<<(std::ostream& os, const Coordinate& coo)
{
    return os << "(" << coo.x << ", " << coo.y << ")";
}


using Coordinates = std::vector<Coordinate>;

struct Star
{
    Coordinate pos{}, speed{};
};

using Data = std::vector<Star>;

std::ostream& operator<<(std::ostream& os, const Star& s)
{
    return os << "[" << s.pos << "] [" << s.speed <<"]";
}

Data read_input()
{
    constexpr int x_pos{10};
    constexpr int x_len{test ? 2 : 6};
    constexpr int y_len{test ? 3 : 7};
    constexpr int dx_len{2};
    constexpr int dy_len{3};
    constexpr int y_pos{x_pos + x_len + 1};
    constexpr int dx_pos{y_pos + y_len + 12};
    constexpr int dy_pos{dx_pos + dx_len + 1};
    Data data;
    Star s{};
    for (std::string line; getline(std::cin, line); ) {
        s.pos = {std::stoi(line.substr(x_pos, x_len)),
            std::stoi(line.substr(y_pos, y_len))};
        s.speed = {std::stoi(line.substr(dx_pos, dx_len)),
            std::stoi(line.substr(dy_pos, dy_len))};
        data.push_back(s);
    }
    return data;
}

Coordinate get_origin(const Coordinates& c) {

	auto x = std::min_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.x < rhs.x;
		});
	auto y = std::min_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.y < rhs.y;
		});
    return {x->x, y->y};

}

std::pair<Coordinate, Coordinate> get_bounding_box(const Coordinates& c) {

	auto x_extreme = std::minmax_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.x < rhs.x;
		});
	auto y_extreme = std::minmax_element(c.begin(), c.end(),
		[](const Coordinate& lhs, const Coordinate& rhs) {
			return lhs.y < rhs.y;
		});
    return std::make_pair(
        Coordinate{x_extreme.first->x, y_extreme.first->y},
        Coordinate {x_extreme.second->x, y_extreme.second->y});

}

Coordinates get_coordinates(const Data& data)
{
    Coordinates result{};
    for (const auto& d: data) {
        result.push_back(d.pos);
    }
    return result;
}
bool draw(const Coordinates& sky)
{
    const auto& [origin, end] = get_bounding_box(sky);
    const auto width = end.x - origin.x + 1;
    const auto height = end.y - origin.y + 1;

    if (height > 10) {
        return false;
    }
    const long long size = width * height;
    if (size >= std::numeric_limits<int>::max()) {
        return false;
    }
    std::string text(size, ' ');
    for (const auto& c: sky) {
        const auto pos = c - origin;
        text.at(pos.y * width + pos.x) = '#';
    }
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            std::cout << text.at(i * width + j);
        }
        std::cout << std::endl;
    }
    return true;
}

void evolve(Data& data)
{
    Coordinates result{};
    for (auto& d: data) {
        d.pos += d.speed;
    }
}


void a(Data& data)
{
    /*
    for (const auto& d: data) {
        std::cout << d << std::endl;
    }
    */
    for (int i = 0; i < 1000000; i++) {
    // for (int i = 0; i < 1; i++) {
    // while (true) {
        auto sky = get_coordinates(data);
        if (draw(sky)) {
            std::cout << i << std::endl;
            break;
        }
        evolve(data);
    }
}

int main()
{
    Data data{read_input()};
    a(data);
}
