#include <iostream>
#include <vector>
#include <stack>
#include <numeric>
#include <utility>
#include <unordered_map>

using Data = std::vector<int>;

enum class State
{
    read_header,
    read_metadata,
    rewind,
};

struct Node
{
    Node (int n) : children_n{n} {};
    int children_n;
    std::vector<Node> children{};
    int metadata_n{0};
    std::vector<int> metadata{};
    std::optional<int> sum_b{std::nullopt};
};

std::ostream& operator<<(std::ostream&os, const std::vector<int>v)
{
    os << "{";
    if (!empty(v)) {
        os << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, const int i) { return a + ", " + std::to_string(i); });
    }
    return os << "}";
}

std::ostream& operator<<(std::ostream& os, const Node& n)
{
    return os << "(" << n.children_n << ", " << n.metadata_n << ") [" << std::accumulate(n.metadata.begin(), n.metadata.end(), 0) << " " << n.metadata << "]";
}

void read_input(Data& data)
{
}

std::pair<Node, int> a()
{
    int v;
    State state{State::read_header};
    Node current{0};
    std::stack<Node> stack{};
    int sum{0};
    while (std::cin >> v) {
        if (state == State::read_header) {
            current = Node(v);
            std::cin >> v;
            current.metadata_n = v;
            // std::cout << current << std::endl;
            if (current.children.size() < current.children_n) {
                stack.push(current);
                state = State::read_header;
            } else {
                state = State::read_metadata;
            }
        } else if (state == State::read_metadata) {
            current.metadata.push_back(v);
            sum += v;
            if (current.metadata.size() == current.metadata_n) {
                state = State::rewind;
            }
        }

        if (state == State::rewind) {
            if (!stack.empty()) {
                auto parent{stack.top()};
                stack.pop();
                parent.children.push_back(current);
                // std::cout << "---> " << current << std::endl;
                current = parent;
                if (!stack.empty()) {
                    // std::cout << "=== " << current << std::endl;
                    // std::cout << "^^^ " << stack.top() << std::endl;
                }
                if (current.children.size() < current.children_n) {
                    stack.push(current);
                    state = State::read_header;
                } else {
                    state = State::read_metadata;
                }
            } else {
                // std::cout << "oh no" << std::endl;
            }
        }
    }
    /*
    while (!stack.empty()) {
        current = stack.top();
        stack.pop();
    }
    */
    return {current, sum};
}

int get_sum(Node& node)
{
    if (node.children_n == 0) {
        std::cout << "0: " << node << std::endl;
        return std::accumulate(node.metadata.begin(), node.metadata.end(), 0);
    }

    int sum{0};
    std::unordered_map<int, int> cache{};
    for (auto i: node.metadata) {
        if (i > 0 && i <= node.children_n) {
            if (cache.count(i)) {
                sum += cache.at(i);
                continue;
            }
            cache[i] = get_sum(node.children.at(i - 1));
            sum += cache[i];
        }
    }
    return sum;
}

int b(Node& tree)
{
    /*
    std::cout << tree << std::endl;
    std::cout << "---" << std::endl;

    for (const auto& node: tree.children) {
        std::cout << node << std::endl;
    }
    */

    return get_sum(tree);
}

int main()
{
    // Data data;
    // read_input(data);
    auto [tree, sum] = a();
    std::cout << sum << std::endl; // 43825
    std::cout << b(tree) << std::endl;
}
