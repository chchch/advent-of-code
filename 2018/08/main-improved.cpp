// based on py https://www.reddit.com/r/adventofcode/comments/a47ubw/2018_day_8_solutions/ebc99t8
// crashes for now...

#include <iostream>
#include <vector>
#include <iterator>
#include <stack>
#include <numeric>

using Data = std::vector<int>;

int b(Data data)
{
    std::stack<int> stack_c{};
    stack_c.push(data.at(0));
    std::stack<int> stack_m{};
    stack_m.push(data.at(1));
    std::stack<int> stack_v = {};
    std::stack<std::vector<int>> children = {};
    children.push({});
    std::stack<std::vector<int>> meta = {};
    meta.push({});

	int i{2};
    while (!stack_c.empty()) {
        if (stack_c.top() > 0) {
            --stack_c.top();
            stack_c.push(data.at(i));
            stack_m.push(data.at(i + 1));
            children.push({});
            meta.push({});
            i += 2;
        } else if (stack_m.top() > 0) {
            meta.top().push_back(data.at(i));
            --stack_m.top();
            ++i;
        } else {
            auto c = children.top();
            children.pop();
            auto m = meta.top();
            meta.pop();
            int v = std::accumulate(m.begin(), m.end(), 0,
                [&c, &m](int a, const int j) {
                    if (c.empty()) {
                        return a + j;
                    } else {
                        return a + (1 <= j <= size(c) ? c.at(j - 1) : 0);
                    }
                });
            if (!children.empty()) {
                children.top().push_back(v);
            } else {
                return v;
            }
            stack_c.pop();
            stack_m.pop();

        }
    }
    return 0;
}

int main()
{
    Data data{std::istream_iterator<int>{std::cin}, {}};

    std::cout << b(data) << std::endl;
}
