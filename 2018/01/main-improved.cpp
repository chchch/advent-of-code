#include <iostream>
#include <iterator>
#include <numeric>

#include <vector>
#include <unordered_set>

int a()
{
    return std::accumulate(std::istream_iterator<int>{std::cin}, {}, 0);
}

int b()
{
    std::vector<int> data{std::istream_iterator<int>{std::cin}, {}};
    std::unordered_set<int> freqs{};
    int sum{0};
    for (int i{0}; freqs.insert(sum += data.at(i)).second;i = (i + 1) % data.size()) {
    }
    return sum;
}

int main()
{
    if (true) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
