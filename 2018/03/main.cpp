#include <iostream>
#include <regex>
#include <numeric>
#include <string>

#include <utility>
#include <unordered_map>
#include <vector>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

// hashing pairs of int:
// https://stackoverflow.com/a/23860042/5239250
// https://stackoverflow.com/a/27952689/5239250

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct IntPairHash {
    size_t operator()(const std::pair<int, int>& p) const {
        return hash_combine(p.first, p.second);
    }
};

// storing (int, int) as one int: g.at((i << 15) | j)

// for unordered_map we need an hashing function;
// for map it's not needed...
using Map = std::unordered_map<std::pair<int, int>, int, IntPairHash>;
using MapId = std::unordered_map<std::pair<int, int>, std::pair<int, std::vector<int>>, IntPairHash>;

int a()
{
    Map fabric{};

    std::regex re(R"(^#(\d+) @ (\d+),(\d+): (\d+)x(\d+))", std::regex_constants::ECMAScript);
    std::smatch match;

    for (std::string line; getline(std::cin, line); ) {
        std::regex_search(line, match, re);
        int id = std::stoi(match[1]);
        int i_0 = std::stoi(match[2]);
        int j_0 = std::stoi(match[3]);
        int n = i_0 + std::stoi(match[4]);
        int m = j_0 + std::stoi(match[5]);
        for (int i = i_0; i < n; i++) {
            for (int j = j_0; j < m; j++) {
                fabric[{i, j}]++;
            }
        }
    }

    return std::accumulate(fabric.begin(), fabric.end(), 0,
        [](int a, auto i) {
            return a + (i.second > 1 ? 1 : 0);
        });
}

int b()
{
    MapId fabric{};

    std::regex re(R"(^#(\d+) @ (\d+),(\d+): (\d+)x(\d+))", std::regex_constants::ECMAScript);
    std::smatch match;

    for (std::string line; getline(std::cin, line); ) {
        std::regex_search(line, match, re);
        int claim_id = std::stoi(match[1]);
        int i_0 = std::stoi(match[2]);
        int j_0 = std::stoi(match[3]);
        int n = i_0 + std::stoi(match[4]);
        int m = j_0 + std::stoi(match[5]);
        for (int i = i_0; i < n; i++) {
            for (int j = j_0; j < m; j++) {
                auto& [count, ids] = fabric[{i, j}];
                count++;
                ids.push_back(claim_id);
            }
        }
    }

    // for (const auto& [pos, value]: fabric) {
    //     std::cout << pos.first << "," << pos.second << " = " << value.first << " [" << value.second << "]" << std::endl;
    // }

    std::map<int, int> claim_overlaps{};
    for (const auto& [pos, value]: fabric) {
        const auto& [count, ids] = value;
        for (const int id: ids) {
            auto& c_o =  claim_overlaps[id];
            if (c_o < count) {
                c_o = count;
            }
        }
    }

    // for (const auto& [id, count]: claim_overlaps) {
    //     std::cout << "#" << id << ": " << count << std::endl;
    // }

    for (const auto& [id, count]: claim_overlaps) {
        if (count == 1) {
            return id;
        }
    }
    return 0;
}

int main()
{
    // std::cout << a() << std::endl;
    std::cout << b() << std::endl;
}
