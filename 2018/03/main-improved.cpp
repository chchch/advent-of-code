#include <iostream>
#include <numeric>
#include <string>

#include <iterator>

#include <utility>
#include <unordered_map>
#include <vector>

// hashing pairs of int:
// https://stackoverflow.com/a/23860042/5239250
// https://stackoverflow.com/a/27952689/5239250

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

struct Claim
{
    int id;
    int x, y, w, h;
};

std::istream& operator>>(std::istream& is, Claim& claim)
{
    char c;
    is >> c >> claim.id >> c >> claim.x >> c >> claim.y >> c >> claim.w >> c >> claim.h;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Claim& c)
{
    return os << c.id << ": " << c.x << "," << c.y << " " << c.w << "x" << c.h;
}

inline size_t hash_combine( size_t lhs, size_t rhs ) {
    lhs^= rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2);
    return lhs;
}

struct IntPairHash {
    size_t operator()(const std::pair<int, int>& p) const {
        return hash_combine(p.first, p.second);
    }
};

// storing (int, int) as one int: g.at((i << 15) | j)

using Map = std::unordered_map<std::pair<int, int>, int, IntPairHash>;
using MapId = std::unordered_map<std::pair<int, int>, std::pair<int, std::vector<int>>, IntPairHash>;

int a()
{
    Map fabric{};
    std::for_each(std::istream_iterator<Claim>{std::cin}, {},
        [&fabric](auto c) {
            for (int i = c.x; i < c.x + c.w; i++) {
                for (int j = c.y; j < c.y + c.h; j++) {
                    fabric[{i, j}]++;
                }
            }
        });

    return std::accumulate(fabric.begin(), fabric.end(), 0,
        [](int a, auto i) {
            return a + (i.second > 1 ? 1 : 0);
        });
}

int b()
{
    MapId fabric{};

    std::for_each(std::istream_iterator<Claim>{std::cin}, {},
        [&fabric](auto c) {
            for (int i = c.x; i < c.x + c.w; i++) {
                for (int j = c.y; j < c.y + c.h; j++) {
                    auto& [count, ids] = fabric[{i, j}];
                    count++;
                    ids.push_back(c.id);
                }
            }
        });

    std::map<int, int> claim_overlaps{};
    for (const auto& [pos, value]: fabric) {
        const auto& [count, ids] = value;
        for (const int id: ids) {
            auto& c_o =  claim_overlaps[id];
            if (c_o < count) {
                c_o = count;
            }
        }
    }

    for (const auto& [id, count]: claim_overlaps) {
        if (count == 1) {
            return id;
        }
    }
    return 0;
}

int main()
{
    // std::cout << a() << std::endl;
    std::cout << b() << std::endl;
}
