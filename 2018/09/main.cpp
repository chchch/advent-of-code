#include <iostream>
#include <iomanip>
#include <string>
#include <numeric>
#include <unordered_map>
#include <algorithm>

const bool test{false};

struct Node
{
    explicit Node(int value) :
        value{value}, next{this}, prev{next} {};
    explicit Node(int value, Node* next, Node* prev) :
        value{value}, next{next}, prev{prev} {};

    Node* append(int value)
    {
        auto node = new Node{value, this->next->next, this->next};
        this->next->next->prev = node;
        this->next->next = node;
        return node;
    }

    void remove()
    {
        this->prev->next = this->next;
        this->next->prev = this->prev;
        delete this;
    }


    int value;
    Node* next;
    Node* prev;
};

std::ostream& operator<<(std::ostream& os, Node* n)
{
    auto i = n;
    os << i->value;
    i = i->next;
    while (i != n) {
        os << std::setw(3) << i->value;
        i = i->next;
    }
    return os;
}

long long a(int player_n, int marble_n)
{
    std::unordered_map<int, long long> player{};
    int player_i = 0;

    auto game = new Node(0);
    auto current = game;

    for (int i = 1; i <= marble_n; i++) {
        if (i % 23 == 0) {
            player[player_i] += i;
            Node* prev = current;
            for (int j = 0; j < 7; j++) {
                prev = prev->prev;
            }
            // std::cout << "---" << prev->value << std::endl;
            player[player_i] += prev->value;
            current = prev->next;
            prev->remove();
        } else {
            current = current->append(i);
        }

        if (test) {
            std::cout << "[" << player_i + 1 << "] " << game << std::endl;
            std::cout << "(" << current->value << ")" << std::endl;
        }

        player_i = (player_i + 1) % player_n;
    }

    return std::max_element(player.begin(), player.end(),
        [](const auto& a, const auto& b) {
            return a.second < b.second;
        })->second;

}

int main()
{
    std::cout << a(9, 25) << std::endl;
    std::cout << a(493, 71863) << std::endl; // 367802 
    std::cout << a(493, 71863 * 100) << std::endl; // 2996043280
}
