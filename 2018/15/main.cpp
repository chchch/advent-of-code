#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <numeric>
#include <sstream>

struct Coordinate
{
    int x, y;
};

std::ostream& operator<<(std::ostream& os, Coordinate c)
{
    return os << "(" << c.x << ", " << c.y << ")";
}

struct Actor
{
    Coordinate pos;
    char type;
    int power{3};
    int points{200};
};

std::ostream& operator<<(std::ostream& os, Actor a)
{
    return os << a.pos << ": " << a.type << " [" << a.power << ", " << a.points << "]";
}

using Map = std::vector<std::string>;
using Actors = std::vector<Actor>;

template <typename T>
std::string to_string(T v)
{
    std::ostringstream ss{};
    ss << v;
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, Actors a)
{
    return os << std::accumulate(a.begin() + 1, a.end(), to_string(*a.begin()),
        [](std::string& a, const auto& i) -> std::string& { a += ", " + to_string(i); return a; }) << std::endl;
}

std::tuple<Map, Actors> read_input()
{
    Map map{};
    Actors actors{};

    int y{0};
    for (std::string line; getline(std::cin, line); ) {
        std::string row{};
        int x{0};
        for (char& c: line) {
            if (c == 'G' || c == 'E') {
                actors.push_back({{x, y}, c});
                c = '.';
            }
            ++x;
        }
        ++y;
        map.push_back(line);
    }
    return {map, actors};
}

int a()
{
    auto [map, actors] = read_input();
    std::cout << actors << std::endl;
    while (true) {
    }
    return 0;
}

int main()
{
    std::cout << a() << std::endl;
}
