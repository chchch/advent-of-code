// a c++ solution: https://www.reddit.com/r/adventofcode/comments/a53r6i/2018_day_11_solutions/ebjogd7
// https://en.wikipedia.org/wiki/Summed-area_table
// a graphical explaination: http://miltomiltiadou.blogspot.com/2013/11/summed-area-tables-explanation-and.html

#include <iostream>
#include <array>
#include <limits>

int get_power_level(int x, int y, int serial_number)
{
    int rack_id{x + 10};
    int power_level{rack_id * y};
    power_level +=  serial_number;
    power_level *= rack_id;
    power_level /= 100;
    power_level %= 10;
    return power_level - 5;
}

int main()
{
    // const int serial_number{18};
    const int serial_number{9445};

	std::array<std::array<int, 301>, 301> sum{};
    int max{std::numeric_limits<int>::min()};
    int max_x{0}, max_y{0}, max_size{0};

    for (int j = 1; j <= 300; j++) {
        for (int i = 1; i <= 300; i++) {
            auto p{get_power_level(i, j, serial_number)};
            sum[j][i] = p + sum[j -1][i] + sum[j][i - 1] - sum[j - 1][i - 1];
        }
    }
    for (int size = 1; size <= 300; size++) { // since we have negative numbers it cannot be 300...
        for (int j = size; j <= 300; j++) {
            for (int i = size; i <= 300; i++) {
                int max_at_size{sum[j][i] - sum[j - size][i] - sum[j][i - size] + sum[j - size][i - size]};
                if (max_at_size > max) {
                    max = max_at_size;
                    max_x = i;
                    max_y = j;
                    max_size = size;
                }
            }
        }
    }
    std::cout << "[" << max_x - max_size + 1 << ", " << max_y - max_size + 1 << "] @ " << max_size << std::endl;
}


/*
int sum[301][301];
signed main() {
    int bx, by, bs, best = INT_MIN;
    for(int y = 1; y <= 300; y++) {
        for(int x = 1; x <= 300; x++) {
            int id = x + 10;
            int p = id * y + 1718;
            p = (p * id) / 100 % 10 - 5;
            sum[y][x] = p + sum[y - 1][x] + sum[y][x - 1] - sum[y - 1][x - 1];
        }
    }
    for(int s = 1; s <= 300; s++) {
        for(int y = s; y <= 300; y++) {
            for(int x = s; x <= 300; x++) {
                int total = sum[y][x] - sum[y - s][x] - sum[y][x - s] + sum[y - s][x - s];
                if(total > best) {
                    best = total, bx = x, by = y, bs = s;
                }
            }
        }
    }
    cout << bx - bs + 1 << "," << by - bs + 1 << "," << bs << endl;
    return 0;
}
*/
