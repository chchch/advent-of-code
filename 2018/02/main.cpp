#include <iostream>
#include <ostream>
#include <istream>
#include <iterator>
#include <tuple>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <numeric>

#include <vector>

std::ostream& operator<<(std::ostream& os, std::tuple<int, int> t)
{
    return os << "{" << std::to_string(std::get<0>(t)) << ", " << std::to_string(std::get<1>(t)) << "}";
}

std::tuple<int, int> a()
{
    return std::accumulate(std::istream_iterator<std::string>{std::cin}, {}, std::make_tuple(0, 0),
        [](std::tuple<int, int> a, std::string str) {
            // std::cout << str << std::endl;
            std::unordered_map<char, int> m{};
            std::for_each(str.begin(), str.end(),
                [&m](char c) {
                    m[c]++;
                });
            bool two = false;
            bool three = false;
            for (auto i: m) {
                if (i.second == 2) {
                    two = true;
                } else if (i.second == 3) {
                    three = true;
                }
            }
            return std::make_tuple(std::get<0>(a) + (two ? 1 : 0), std::get<1>(a) + (three ? 1 : 0));
        });
}

std::string b()
{
    std::vector<std::string> data{std::istream_iterator<std::string>{std::cin}, {}};
    for (int i = 0; i < data.size(); i++) {
        for (int j = i + 1; j < data.size(); j++) {
            int difference{-1};
            auto it_a = data.at(i);
            auto it_b = data.at(j);
            for (int k = 0; k < a.size(); k++) {
                if (a.at(k) != b.at(k)) {
                    if (difference >= 0) {
                        difference = -1;
                        break;
                    } else {
                        difference = k;
                    }
                }
            }
            if (difference >= 1) {
                return a.substr(0, difference) + a.substr(difference + 1, a.size());
            }
        }
    }
    return {};
}

int main()
{
    if (false) {
        auto r = a();
        std::cout << r << " = " << std::get<0>(r) * std::get<1>(r) << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
