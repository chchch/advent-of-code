#include <iostream>
#include <iterator>
#include <numeric>

#include <vector>
#include <queue>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

using World = std::vector<std::vector<int>>;

int a()
{
    World world;
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<int> row{};
        for (char c: line) {
            row.push_back(c - '0');
        }
        world.push_back(row);
    }
    // for (auto line: world) {
    //     std::cout << line << std::endl;
    // }
    int n = world.size();
    int m = world.at(0).size();
    // std::cout << n << " " << m << std::endl;
    int risk_level = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            int v = world.at(i).at(j);
            if (
                (j == 0 || world.at(i).at(j - 1) > v) &&
                (j >= m - 1 || world.at(i).at(j + 1) > v) &&
                (i == 0 || world.at(i - 1).at(j) > v) &&
                (i >= n - 1 || world.at(i + 1).at(j) > v)
            ) {
                // std::cout << v << std::endl;
                risk_level += v + 1;
            }
        }
    }
    return risk_level; // 197 too low
}

struct Coord {
    int i, j;
};

std::string to_string(Coord p) {
    return "(" + std::to_string(p.i) + ", " + std::to_string(p.j) + ")";
}

std::ostream& operator<<(std::ostream& os, const std::vector<Coord>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), to_string(*v.begin()),
        [](std::string a, Coord c) { return a + ", " + to_string(c); }) << "}";
}

std::vector<Coord> get_neighbors(World& world, Coord p) {
    std::vector<Coord> neighbors{};
    int n = world.size();
    int m = world.at(0).size();
    if (p.i > 0) {
        neighbors.push_back({p.i - 1, p.j});
    }
    if (p.i < world.size() - 1) {
        neighbors.push_back({p.i + 1, p.j});
    }
    if (p.j > 0) {
        neighbors.push_back({p.i, p.j - 1});
    }
    if (p.j < world.at(0).size() - 1) {
        neighbors.push_back({p.i, p.j + 1});
    }
    return neighbors;
}

// skip if it's a 9 or it's already in a basin
bool skip(const World& world, const World& basins, const Coord& c) {
    return (world.at(c.i).at(c.j) == 9) || (basins.at(c.i).at(c.j) != 0);
    
}

int b()
{
    World world;
    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        std::vector<int> row{};
        for (char c: line) {
            row.push_back(c - '0');
        }
        world.push_back(row);
    }

    // std::cout << get_neighbors(world, Coord{0, 0}) << std::endl;
    // std::cout << get_neighbors(world, Coord{4, 9}) << std::endl;
    // std::cout << get_neighbors(world, Coord{3, 3}) << std::endl;

    World basins(world.size(), std::vector<int>(world.at(0).size()));
    std::vector<int> basin_sizes{};
    int current_basin = 0;
    for (int i = 0; i < world.size(); i++) {
        for (int j = 0; j < world.at(0).size(); j++) {
            if (skip(world, basins, {i, j})) {
                continue;
            }
            current_basin++;
            std::queue<Coord> potential_in_baisin{};
            potential_in_baisin.push({i, j});
            while (!potential_in_baisin.empty()) {
                auto location = potential_in_baisin.front();
                potential_in_baisin.pop();
                if (skip(world, basins, location)) {
                    continue;
                }
                basins.at(location.i).at(location.j) = current_basin;
                if (basin_sizes.size() < current_basin) {
                    basin_sizes.push_back(0);
                }
                basin_sizes.at(current_basin - 1)++;
                for (const auto& c: get_neighbors(world, location)) {
                    if (skip(world, basins, c)) {
                        continue;
                    }
                    potential_in_baisin.push(c);
                }
            }
        }
    }
    // for (const auto& row: basins) {
    //     std::cout << row << std::endl;
    // }
    std::sort(basin_sizes.begin(), basin_sizes.end());
    // std::cout << basin_sizes << std::endl;
    return std::accumulate(basin_sizes.end() - 3, basin_sizes.end(), 1, [](int a, int v) {
        return a * v;
    });
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
