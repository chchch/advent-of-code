#include <iostream>
#include <iterator>
#include <numeric>
#include <cmath>

#include <vector>
#include <stack>
#include <unordered_map>

#include <string>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

int a()
{
    int score{0};

    std::unordered_map<char, char> braces{
        {'(', ')'},
        {'{', '}'},
        {'[', ']'},
        {'<', '>'},
    };

    std::unordered_map<char, int> points{
        {')', 3},
        {']', 57},
        {'}', 1197},
        {'>', 25137},
    };

    std::string line;
    std::stack<char> levels{};
    for (std::string line; std::getline(std::cin, line); ) {
        // std::cout << line << std::endl;
        for (char c: line) {
            if (braces.contains(c)) {
                levels.push(c);
            } else {
                char opening_char = levels.top();
                levels.pop();
                if (braces.at(opening_char) != c) {
                    score += points.at(c);
                }
            }
        }
    }
    return score;
}


unsigned long long int  b()
{
    std::vector<unsigned long long int> scores{};

    std::unordered_map<char, char> braces{
        {'(', ')'},
        {'{', '}'},
        {'[', ']'},
        {'<', '>'},
    };

    std::unordered_map<char, int> points{
        {'(', 1},
        {'[', 2},
        {'{', 3},
        {'<', 4},
    };

    std::string line;
    for (std::string line; std::getline(std::cin, line); ) {
        // std::cout << "line: " << line << std::endl;
        std::stack<char> levels{};
        for (char c: line) {
            if (braces.contains(c)) {
                levels.push(c);
            } else {
                char opening_char = levels.top();
                levels.pop();
                if (braces.at(opening_char) != c) {
                    levels = {};
                    break;
                }
            }
        }
        unsigned long long int level_score{0};
        while (!levels.empty()) {
            level_score *= 5;
            char opening_char = levels.top();
            levels.pop();
            level_score += points.at(opening_char);
        }
        if (level_score > 0) {
            scores.push_back(level_score);
        }
    }
    // std::cout << scores << std::endl;
    std::sort(scores.begin(), scores.end());
    return scores.at(scores.size() / 2);
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
