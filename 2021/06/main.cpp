#include <iostream>
#include <sstream>
#include <iterator>

#include <unordered_map>
#include <algorithm>
#include <numeric>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}

std::vector<int> split(std::string text, char delimiter) {
    std::vector<int> tokens{};
    std::istringstream iss(text);
    std::string token;
    while(std::getline(iss, token, delimiter)) {
        tokens.push_back(std::stoi(token));
    }
    return tokens;
}

int a(std::vector<int> fishes)
{
    // std::cout << fishes << std::endl;
    for (int i = 0; i < 80; i++) {
        int zeroes = std::count_if(fishes.begin(), fishes.end(), [](int v)
            { return v == 0; });
        // std::cout << zeroes << std::endl;
        std::transform(fishes.begin(), fishes.end(), fishes.begin(), [](int v) {
            if (v == 0) {
                return 6;
            } else {
                return v - 1;
            }
        });
        auto new_fishes = std::vector<int>(zeroes, 8);
        fishes.insert(fishes.end(), new_fishes.begin(), new_fishes.end());
        // std::cout << fishes << std::endl;
    }
    return fishes.size();
}

using Fish_counter = std::unordered_map<int, unsigned long long int>;

unsigned long long int b(std::vector<int> fishes)
{
    // std::cout << fishes << std::endl;
    Fish_counter fish_counter{};
    for (auto fish: fishes) {
        fish_counter[fish]++;
    }
    for (int i = 0; i < 256; i++) {
        Fish_counter next_counter{};
        for ( const auto &[key, value]: fish_counter ) {
            if (key == 0) {
                next_counter[8] += value;
                next_counter[6] += value;
            } else {
                next_counter[key - 1] += value;
            }
        }
        fish_counter = next_counter;
    }
    return std::accumulate(fish_counter.begin(), fish_counter.end(), 0, [](unsigned long long int a, const Fish_counter::value_type& p) {
        return a + p.second;
    });
}

int main()
{
    std::string test = "3,4,3,1,2";
    std::string input = "5,1,4,1,5,1,1,5,4,4,4,4,5,1,2,2,1,3,4,1,1,5,1,5,2,2,2,2,1,4,2,4,3,3,3,3,1,1,1,4,3,4,3,1,2,1,5,1,1,4,3,3,1,5,3,4,1,1,3,5,2,4,1,5,3,3,5,4,2,2,3,2,1,1,4,1,2,4,4,2,1,4,3,3,4,4,5,3,4,5,1,1,3,2,5,1,5,1,1,5,2,1,1,4,3,2,5,2,1,1,4,1,5,5,3,4,1,5,4,5,3,1,1,1,4,5,3,1,1,1,5,3,3,5,1,4,1,1,3,2,4,1,3,1,4,5,5,1,4,4,4,2,2,5,5,5,5,5,1,2,3,1,1,2,2,2,2,4,4,1,5,4,5,2,1,2,5,4,4,3,2,1,5,1,4,5,1,4,3,4,1,3,1,5,5,3,1,1,5,1,1,1,2,1,2,2,1,4,3,2,4,4,4,3,1,1,1,5,5,5,3,2,5,2,1,1,5,4,1,2,1,1,1,1,1,2,1,1,4,2,1,3,4,2,3,1,2,2,3,3,4,3,5,4,1,3,1,1,1,2,5,2,4,5,2,3,3,2,1,2,1,1,2,5,3,1,5,2,2,5,1,3,3,2,5,1,3,1,1,3,1,1,2,2,2,3,1,1,4,2";
    if (false) {
        std::cout << a(split(test, ',')) << std::endl;
        std::cout << a(split(input, ',')) << std::endl;

    } else {
        std::cout << b(split(test, ',')) << std::endl;
        std::cout << b(split(input, ',')) << std::endl;
    }
}
