#!/usr/bin/env bash

# Usage:
# - set USE_SAN to any value to use the sanitizers
# - set SAN, OPTIM, CPP or CFLAGS to override the default settings

set -eu

prefix=

if [ "${USE_SAN-no}" = no ]; then
    SAN=${SAN-}
else
    SAN=${SAN--fsanitize=undefined -fsanitize=address -lubsan}
    prefix=san-$prefix
fi

OPTIM=${OPTIM--O2}
CFLAGS=${CFLAGS--fdiagnostics-color $OPTIM -Wall -gdwarf-4 -g3 -lstdc++}
CPP=${CPP-g++}

defines=
if [ -n "${UNSAFE-}" ]; then
    defines=-DUNSAFE
    prefix=unsafe-$prefix
fi
DEFINES=${DEFINES-$defines}

out=$prefix${PWD##*/}

if [ ! main.cpp -ot $out ]; then
    set -x
    $CPP $CFLAGS $DEFINES $SAN -std=c++20 -o $out main.cpp
    set +x
fi

for t in test*.txt; do
    echo "Test $t:"
    time ./$out < "$t" > result-"$t"
done

status=$(git status -s result-test*.txt)

if [ -n "$status" ]; then
    printf 'Test failures:\n%s\n' "$status"
    false
fi

