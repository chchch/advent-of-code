#include <iostream>
#include <iterator>
#include <numeric>
#include <cmath>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <bitset>
#include <string>
#include <cstdint>
#include <cassert>

// Let the compiler resolve the choice of an overloaded function
// automatically.  Requires C++14.  Taken from:
// https://stackoverflow.com/questions/2942426/how-do-i-specify-a-pointer-to-an-overloaded-function
#define RESOLVE(func)                                                   \
    [&](auto&&... args) -> decltype(                                    \
        func(std::forward<decltype(args)>(args)...))                    \
    {                                                                   \
        return func(std::forward<decltype(args)>(args)...);             \
    }

namespace mylib_std {
    // Providing this as part of a possible future utility library
    
    // std::isupper takes a signed integer, and char can't be directly
    // passed to it if it can contain 8-bit characters. See
    // https://en.cppreference.com/w/cpp/string/byte/isupper#Notes
    static
    bool isupper(char c) {
        // std::cerr << "isupper('" << c << "')" << std::endl;
        return std::isupper((unsigned char)c);
    }
}

// Just import that, so simply using isupper with char will actually
// use the converting one (this relies on nobody else providing an
// alternative definition of the same, and on there not being possible
// detrimental effects from overloading to a different isupper than
// inteneded just because of the existence of the new definition; the
// safe way would be not to import but to fully qualify; note that
// non-namespaced isupper from C might always be present):
using mylib_std::isupper;

static
bool isupper(const std::string& string) {
    return std::all_of(string.begin(), string.end(), RESOLVE(isupper));
}

template<typename T>
static
bool contains(const std::vector<T>& haystack,
              const T needle) {
    return (std::find(haystack.begin(), haystack.end(), needle)
            != haystack.end());
}


#define _SYMBOLIDS_BITS 16
typedef uint16_t symbolIds_t;
// could go to int32_t on 64-bit systems, but computational complexity
// would likely foil it anyway.

typedef symbolIds_t symbolId_t;
typedef symbolIds_t symbolTableId_t;

class Symbol {
public:
    Symbol() : id_symbolTableId_isupper_(0) {}
private:
    // Allow nobody but SymbolTable to create Symbol objects)
    Symbol(symbolId_t id, symbolTableId_t tid, bool is_upper) {
        id_symbolTableId_isupper_ =
            ( id
              | (tid << _SYMBOLIDS_BITS)
              | (is_upper ? (1 << (2 * _SYMBOLIDS_BITS - 1)) : 0));
        assert(this->id() == id);
        assert(this->symbolTableId() == tid);
        assert(this->is_upper() == is_upper);
    }
    friend class SymbolTable;
public:
    symbolId_t id() const {
        return id_symbolTableId_isupper_;
    }
    bool is_upper() const {
        return id_symbolTableId_isupper_ & (1 << (2 * _SYMBOLIDS_BITS - 1));
    }
    bool is_lower() const {
        return !is_upper();
    }
    symbolTableId_t symbolTableId() const {
        return ((id_symbolTableId_isupper_ >> _SYMBOLIDS_BITS)
                & ((1 << (_SYMBOLIDS_BITS - 1)) - 1));
    }
    bool operator==(const Symbol &that) const {
        return id_symbolTableId_isupper_ == that.id_symbolTableId_isupper_;
    }
    // (operator!= is derived automatically via C++20)
private:
    // For a version that uses struct bitfields instead, see Git
    // history
    /*const*/ uint32_t id_symbolTableId_isupper_;
};


// Max number of symbols; NOTE: for values larger than 64, reverting
// 88cebd2599566e2630ab36027ccf36ac2d28c8d7 makes it faster. (For
// dynamic sizing, revert eef237a3b3254b5cdbf10101777d5ac8bc1048c8.)
#define SYMBOLSET_SIZE 64

class SymbolSet {
private:
    SymbolSet(size_t siz, symbolTableId_t symbolTableId)
#ifndef UNSAFE
        : symbolTableId_(symbolTableId)
#endif
    {
        assert(siz <= SYMBOLSET_SIZE);
    }
    friend class SymbolTable;
public:
    void add(Symbol s) {
#ifndef UNSAFE
        assert(s.symbolTableId() == symbolTableId_);
#endif
        set_[s.id()] = true;
    }
    void remove(Symbol s) {
#ifndef UNSAFE
        assert(s.symbolTableId() == symbolTableId_);
#endif
        set_[s.id()] = false;
    }
    bool contains(Symbol s) const {
#ifndef UNSAFE
        assert(s.symbolTableId() == symbolTableId_);
#endif
        return set_[s.id()];
    }
private:
    // membership, symbolId -> bool
    std::bitset<SYMBOLSET_SIZE> set_;
#ifndef UNSAFE
    const symbolTableId_t symbolTableId_;
#endif
};


symbolTableId_t next_SymbolTable_id() {
    static symbolTableId_t symbolTable_counter { 0 };
    if (symbolTable_counter < ((1<<15) - 1)) {
        return symbolTable_counter++;
    } else {
        throw std::logic_error("too many SymbolTable:s allocated");
    }
}

class SymbolTable {
public:
    // copies s if not contained yet
    Symbol interned(const std::string &s) {
        assert(! frozen);
        auto it { interned_strings_.find(s) };
        if (it == interned_strings_.end()) {
            Symbol sym { count_++, id_, isupper(s) };
            assert(count_); // check for wrap-around
            interned_strings_[s] = sym;

            auto sym_id { sym.id() };
            if (sym_id >= string_of_.size())
                string_of_.resize(sym_id + 1);
            string_of_[sym.id()] = s;

            return sym;
        } else {
            return it->second;
        }
    }
    SymbolSet symbolSet() const {
        assert(frozen);
        return SymbolSet(count_, id_);
    }
    void freeze() {
        frozen = true; // stop creating more symbols, please
    }
    const std::string string_of(Symbol sym) {
        return string_of_[sym.id()];
    }
private:
    const uint16_t id_ { next_SymbolTable_id() };
    bool frozen { false };
    symbolId_t count_ { 1 }; // 0 is reserved for uninitialized symbols
    std::unordered_map<std::string, Symbol> interned_strings_ {};
    std::vector<std::string> string_of_ {};
};


class Cave {
public:
    Cave() {}
    void add_to(Symbol to) {
        tos_.push_back(to);
    }
    auto & tos() const {
        return tos_;
    }
    bool contains_to(Symbol to) {
        return contains(tos_, to);
    }
private:
    // for iteration (and in original order, even though that fact is
    // not used), as well as lookup, since lookup only happens at
    // startup:
    std::vector<Symbol> tos_;
};

class Caves {
public:
    void add_connection(const std::string &from,
                        const std::string &to,
                        SymbolTable &symbols) {
        auto From { symbols.interned(from) };
        auto To { symbols.interned(to) };
        if (From.id() >= caves_.size()) {
            caves_.resize(From.id() + 1);
        }
        auto &cave { caves_[From.id()] };
        if (cave.contains_to(To)) {
            std::cerr << "Ignoring duplicate connection from "
                      << from << " to "
                      << to << std::endl;
        } else {
            cave.add_to(To);
        }
    }
    const Cave & at(Symbol from) const {
        return caves_.at(from.id());
    }
private:
    std::vector<Cave> caves_ {};
};


static
Caves get_caves(SymbolTable &symbols) {
    Caves caves{};

    std::string line, left, right;
    while (std::getline(std::cin, line)) {
        std::istringstream iss(line);
        std::getline(iss, left, '-');
        std::getline(iss, right, '-');
        caves.add_connection(left, right, symbols);
        caves.add_connection(right, left, symbols);
    }

    return caves;
}


class Context {
public:
    Context () {
        symbols.freeze();
    }
    SymbolTable symbols {};
    Caves caves { get_caves(symbols) };
    Symbol start { symbols.interned("start") };
    Symbol end { symbols.interned("end") };
};

static
int go(const Context &ctx,
       Symbol from,
       SymbolSet set) {
    if (from.is_upper() || !set.contains(from)) {
        if (from == ctx.end) {
            return 1;
        }
        set.add(from);
        int path_counter { 0 };
        for (const auto &to: ctx.caves.at(from).tos()) {
            path_counter +=  go(ctx, to, set);
        }
        return path_counter;
    }
    return 0;
}

static
int go_b(const Context &ctx,
         Symbol from,
         SymbolSet set,
         bool used_lower_dupe) {
    bool using_dupe;
    if (from.is_upper() || !set.contains(from)) {
        using_dupe = false; goto recurse;
    }
    // else it's lowercase, and already in set
    if (from != ctx.start && !used_lower_dupe) {
        using_dupe = true; goto recurse;
    }
    return 0;
recurse:
    if (from == ctx.end) {
        return 1;
    }
    if (!using_dupe) set.add(from);
    int path_counter { 0 };
    for (const auto &to: ctx.caves.at(from).tos()) {
        path_counter +=  go_b(ctx, to, set,
                              using_dupe || used_lower_dupe);
    }
    return path_counter;
}

static
int a(const Context &ctx)
{
    auto set { ctx.symbols.symbolSet() };
    return go(ctx, ctx.start, set);
}

static
int b(const Context &ctx)
{
    auto set { ctx.symbols.symbolSet() };
    return go_b(ctx, ctx.start, set, false);
}

int main()
{
    // std::cerr << "sizeof(Symbol) = " << sizeof(Symbol) << std::endl;
    Context ctx {};
    std::cout << a(ctx) << std::endl;
    std::cout << b(ctx) << std::endl;
}
