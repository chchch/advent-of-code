#include <iostream>
#include <iterator>

#include <bitset>
#include <array>
#include <numeric>

#include <vector>
#include <algorithm>

// const int n_bits = 5;
const int n_bits = 12;

struct Frequency {
    int n;
    std::array<int, n_bits> counter;
};

int a()
{
    auto frequency = std::accumulate(std::istream_iterator<std::bitset<n_bits>>{std::cin}, {}, Frequency{}, [](Frequency a, std::bitset<n_bits> v) {
        a.n++;
        for (int i = 0; i < n_bits; i++) {
            a.counter[i] += v.test(i);
        }
        return a;
    });
    std::bitset<n_bits>gamma{};
    for (int i = 0; i < n_bits; i++) {
        gamma.set(i, frequency.counter[i] > frequency.n / 2);
    }
    std::cout << gamma << std::endl;
    return gamma.to_ulong() * gamma.flip().to_ulong();
}

using BitRow = std::bitset<n_bits>;
using BitList = std::vector<BitRow>;

BitRow get_last_row(BitList data, bool keep_most_common = true) {
    auto oxygen = data;
    for (int i = 0; i < n_bits; i++) {
        int n = std::accumulate(data.begin(), data.end(), 0, [i](int a, std::bitset<n_bits> v) {
            return a + v.test(n_bits - i - 1);
        });
        bool ones_win = n >= (float) data.size() / 2.0;
        // std::cout << "ones_win " << i << ": " <<  n  << " / " << data.size() << " = " << ones_win << std::endl;
        auto data_end = std::remove_if(data.begin(), data.end(), [i, ones_win, keep_most_common](std::bitset<n_bits> v) {
            return v.test(n_bits - i - 1) == ones_win ^ keep_most_common;
        });
        data.erase(data_end, data.end());
        // std::cout << "size " << i << " " << data.size() << std::endl;
        if (data.size() == 1) {
            break;
        }
    }

    // std::cout << "size " << data.size() << std::endl;
    // std::cout << data.at(0) << std::endl;
    return data.at(0);
}

int b()
{
    BitList data((std::istream_iterator<std::bitset<n_bits>>{std::cin}),
                           std::istream_iterator<std::bitset<n_bits>>{});

    auto oxygen = get_last_row(data);
    auto co2 = get_last_row(data, false);

    return oxygen.to_ulong() * co2.to_ulong();
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
