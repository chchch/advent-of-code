from dataclasses import dataclass

@dataclass
class Point:
    x: int
    y: int

@dataclass
class Line:
    start: Point
    end: Point
    @staticmethod
    def from_stdio(self, line):
        start, end = split(' -> ')]
        return Line(Point.from_stdio(x), Point.from_stdio(y) for x, y in 
        


def directed_range(a, b):
    if b >= a:
        return range(a, b + 1)
    else:
        return range(a , b - 1, -1)

def b(filename):
    ventsMap = {}
    with open(filename) as file:
        for line in file:
            # 0,9 -> 5,9
            # print(line.rstrip())
            # coord = [(Point(start.split(',')), Point(end.split(','))) for start, end in line.rstrip().split(' -> ')]
            coord = [list(map(int, c.split(','))) for c in line.rstrip().split(' -> ')]
            # print(coord)
            if coord[0][0] == coord[1][0]:
                for y in directed_range(coord[0][1], coord[1][1]):
                    ventsMap.setdefault((coord[0][0], y), 0)
                    ventsMap[(coord[0][0], y)] += 1
            elif coord[0][1] == coord[1][1]:
                for x in directed_range(coord[0][0], coord[1][0]):
                    ventsMap.setdefault((x, coord[0][1]), 0)
                    ventsMap[(x, coord[0][1])] += 1
            else:
                for x, y in zip(directed_range(coord[0][0], coord[1][0]), directed_range(coord[0][1], coord[1][1])):
                    ventsMap.setdefault((x, y), 0)
                    ventsMap[(x, y)] += 1

    # print(ventsMap)
    # for y in range(10):
    #     for x in range(10):
    #         print(ventsMap.get((x, y), ' '), end=' ')
    #     print()
    print(sum([1 if c >= 2 else 0 for c in ventsMap.values()]))


def main():
    b('test.txt')
        
if __name__ == "__main__":
    main()
