#include <iostream>
#include <iterator>
#include <numeric>

#include <string>

struct Command {
    std::string direction;
    int distance;

};

std::istream &operator>>(std::istream& is, Command& c) {
    is >> c.direction >> c.distance;
    return is;
}

std::ostream& operator<<(std::ostream& os, const Command& c)
{
    return os << c.direction << " / " << c.distance;
}

struct Position {
    int x;
    int y;
};

std::ostream& operator<<(std::ostream& os, const Position& p)
{
    return os << p.x << ", " << p.y;
}

struct PositionWithAim {
    int x;
    int y;
    int aim;
};

std::ostream& operator<<(std::ostream& os, const PositionWithAim& p)
{
    return os << p.x << ", " << p.y << " ( " << p.aim << ")";
}

int a()
{
    Position p = std::accumulate(std::istream_iterator<Command>{std::cin}, {}, Position{0, 0}, [](Position a, Command c) {
        // std::cout << c << std::endl;
        if (c.direction == "forward") {
            a.x += c.distance;
        } else if (c.direction == "up") {
            a.y -= c.distance;
        } else if (c.direction == "down") {
            a.y += c.distance;
        }
        return a;
    });
    // std::cout << p << std::endl;
    return p.x * p.y;
}

int b()
{
    PositionWithAim p = std::accumulate(std::istream_iterator<Command>{std::cin}, {}, PositionWithAim{0, 0, 0}, [](PositionWithAim a, Command c) {
        // std::cout << c << std::endl;
        if (c.direction == "forward") {
            a.x += c.distance;
            a.y += a.aim * c.distance;
        } else if (c.direction == "up") {
            a.aim -= c.distance;
        } else if (c.direction == "down") {
            a.aim += c.distance;
        }
        return a;
    });
    // std::cout << p << std::endl;
    return p.x * p.y;
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
