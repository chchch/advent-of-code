#include <iostream>
#include <iterator>
#include <numeric>

#include<limits>

#include <vector>
#include <deque>

int a()
{
    int prec = std::numeric_limits<int>::max();
    return std::count_if(std::istream_iterator<int>{std::cin}, {}, [&prec](int v) {
        bool r = v > prec;
        prec = v;
        return r;
    });
}

int b()
{
    std::deque<int> window{};
    int old_sum{0};
    int sum{0};
    int count{0};
    int i{0};
    return std::count_if(std::istream_iterator<int>{std::cin}, {}, [&](int v) {
        window.push_back(v);
        sum = old_sum + v;
        if (i > 2) {
            sum -= window.front();
            window.pop_front();
        }
        bool r = i > 2 && sum > old_sum;
        old_sum = sum;
        i++;
        return r;
    });
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
