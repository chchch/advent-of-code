#include <iostream>
#include <iterator>
#include <numeric>

#include <sstream>

#include <vector>
#include <unordered_map>
#include <utility>

#include <string>
#include <algorithm>

int a() {
    int counter = 0;
    for (std::string line; std::getline(std::cin, line); ) {
        std::cout << line << std::endl;

        std::string left, right;
        {
            std::istringstream iss(line);
            std::getline(iss, left, '|');
            std::getline(iss, right, '|');
            // std::cout << right << std::endl;
        }
        {
            std::istringstream iss(right);
            counter +=  std::count_if(std::istream_iterator<std::string>{iss}, {}, [](std::string v) {
                int n = v.size();
                // std::cout << v << n << std::endl;
                return n == 2 || n == 4 || n == 3 || n == 7;
            });
        }
    };
    return counter;
}

std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& v)
{
    return os << "{" << std::accumulate(v.begin() + (v.empty() ? 0 : 1), v.end(), v.empty() ? "" : *v.begin(),
        [](std::string a, std::string v) { return a + ", " + v; }) << "}";
}

bool str_contains(std::string s, char c) {
    return s.find(c) != std::string::npos;
}

auto get_leds(const std::string& line) {
    std::string left, right;

    {
        std::istringstream iss(line);
        std::getline(iss, left, '|');
        std::getline(iss, right, '|');
    }
    // std::cout << "left " << left << std::endl;
    // std::cout << "right " << right << std::endl;
    std::vector<std::string> left_tokens;
    {
        std::istringstream iss(left);
        std::copy(std::istream_iterator<std::string>(iss), {},
             std::back_inserter(left_tokens));
    }
    std::vector<std::string> right_tokens;
    {
        std::istringstream iss(right);
        std::copy(std::istream_iterator<std::string>(iss), {},
             std::back_inserter(right_tokens));
    }
    
    std::for_each(left_tokens.begin(), left_tokens.end(), [](auto& v) {
        std::sort(v.begin(), v.end());
    });
    std::for_each(right_tokens.begin(), right_tokens.end(), [](auto& v) {
        std::sort(v.begin(), v.end());
    });

    return std::make_tuple(left_tokens, right_tokens);
}

auto get_by_length(const std::vector<std::string>& leds, int length) {
    std::vector<std::string> matching{};
    std::copy_if (leds.begin(), leds.end(), std::back_inserter(matching), [length](std::string v){return v.size() == length;} );
    // std::cout << "matching: " << matching << std::endl;

    return matching;
}

auto get_intersection(const std::string& a, const std::string& b) {
    std::string a_and_b{};
    std::set_intersection(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(a_and_b));
    return a_and_b;
}

auto get_difference(const std::string& a, const std::string& b) {
    std::string a_and_b{};
    std::set_difference(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(a_and_b));
    return a_and_b;
}

int b() {

    // 2 -> 1
    //    
    //   |
    //    
    //   |
    //      -   - 
    // 4 -> 4
    //    
    // | |
    //  - 
    //   |
    //    
    // 3 -> 7
    //  - 
    //   |
    //    
    //   |
    //    
    // 7 -> 8
    //  - 
    // | |
    //  - 
    // | |
    //  - 
    // 5 -> 2, 3, 5
    //  -   -   - 
    //   |   | |  
    //  -   -   - 
    // |     |   |
    //  -   -   - 
    // 6 -> 0, 6, 9
    //  -   -   - 
    // | | |   | |
    //      -   -  
    // | | | |   | 
    //  -   -   - 
    //
    //  a
    // b c
    //  d
    // e f
    //  g

    int counter = 0;
    for (std::string line; std::getline(std::cin, line); ) {
        // std::cout << line << std::endl;

        auto [patterns, output] = get_leds(line);

        // std::cout << "patterns: " << patterns << std::endl;
        // std::cout << "output: " << output << std::endl;

        auto one = get_by_length(patterns, 2).at(0);
        auto seven = get_by_length(patterns, 3).at(0);
        auto four = get_by_length(patterns, 4).at(0);
        auto eight = get_by_length(patterns, 7).at(0);

        std::string zero = "";
        std::string two = "";
        std::string three = "";
        std::string five = "";
        std::string six = "";
        std::string nine = "";

        {
            auto five_leds = get_by_length(patterns, 5);
            for (const auto& pattern: five_leds) {
                auto diff = get_intersection(one, pattern);
                if (diff.size() == 2) {
                    three = pattern;
                } else {
                    auto diff = get_intersection(four, pattern);
                    if (diff.size() == 2) {
                        two = pattern;
                    } else {
                        five = pattern;
                    }
                }
            }
        }

        {
            auto six_leds = get_by_length(patterns, 6);
            for (const auto& pattern: six_leds) {
                auto diff = get_intersection(five, pattern);
                if (diff.size() != five.size()) {
                    zero = pattern;
                } else {
                    auto diff = get_intersection(one, pattern);
                    if (diff.size() == 1) {
                        six = pattern;
                    } else {
                        nine = pattern;
                    }
                }
            }
        }

        std::unordered_map<std::string, int>  numbers{
            {zero, 0}, {one, 1}, {two, 2},
            {three, 3}, {four, 4}, {five, 5},
            {six, 6}, {seven, 7}, {eight, 8},
            {nine, 9}
        };

        counter += std::accumulate(output.begin() + 1, output.end(), numbers.at(*output.begin()), [numbers](int a, const auto& v) {
            return a * 10 + numbers.at(v);
        });
    }

    return counter;
}

int main()
{
    if (false) {
        std::cout << a() << std::endl;
    } else {
        std::cout << b() << std::endl;
    }
}
