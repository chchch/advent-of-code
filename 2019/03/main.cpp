/**
 * NOT FINISHED
 */
#include <map>
#include <utility>

#include <sstream>
#include <iterator>
#include <string>
#include <iostream>
#include <vector>

#include <numeric>

#include <fstream>
#include <functional>
#include <algorithm>

std::ostream& operator<<(std::ostream& os, const std::vector<int>& v)
{
    return os << "{" << std::accumulate(v.begin() + 1, v.end(), std::to_string(*v.begin()),
        [](std::string a, int i) { return a + ", " + std::to_string(i); }) << "}";
}



using Point = std::pair<int, int>;
using Points = std::map<Point, int>;
using Move = std::pair<char, int>;
using Moves = std::vector<Move>;

std::ostream& operator<<(std::ostream& os, const Point& v)
{
    return os << "(" << v.first << ", " << v.second << ")";
}

std::map<char, std::function<std::pair<int, int>(std::pair<int, int>)>> moving = {
    {'U', [](Point p) -> Point { return {p.first, p.second + 1};}},
    {'R', [](Point p) -> Point { return {p.first + 1, p.second};}},
    {'D', [](Point p) -> Point { return {p.first, p.second - 1};}},
    {'L', [](Point p) -> Point { return {p.first - 1, p.second};}}
};

Points get_points(Moves moves)
{
    Points result{};
    Point pos{0, 0};
    int length = 0;
    for (auto move: moves) {
        for (int i = 0; i < move.second; i++) {
            pos = moving.at(move.first)(pos);
            length += 1;
            result.insert({pos, length});
        }
    }
    return result;
}

int main()
{
    std::ifstream infile("input.txt");

    std::vector<Points> paths{};

    std::string line;
    while (infile >> line)
    {
        Moves moves{};
        std::string delimiter = ",";
        size_t pos = 0;
        std::string token;
        while ((pos = line.find(delimiter)) != std::string::npos) {
            token = line.substr(0, pos);
            moves.push_back({token.at(0), std::stoi(token.substr(1))});
            line.erase(0, pos + delimiter.length());
        }
        paths.push_back(get_points(moves));
    }

	Points intersection;
	std::set_intersection(
        paths.at(0).begin(), paths.at(0).end(), 
        paths.at(1).begin(), paths.at(1).end(), 
        inserter(intersection, intersection.begin()),
        [](const auto &a, const auto &b)
             { return (a.second == b.second) ? a.first < b.first : a.second < b.second; });

        for (const auto &point : intersection) {
            std::cout << point.first << std::endl;
        }

    // auto sum_of_abs = [](const auto& xy) {return std::abs(xy.first) + std::abs(xy.second);};
    // std::cout << 1 << std::min(std::accumulate(
    //     std::next(intersection.begin()), intersection.end(), sum_of_abs(intersection.begin()),
    //     sum_of_abs
    // )) << std::endl;


    // std::cout << a() << std::endl;
}
