paths = []
with open('input.txt', 'r') as f:
    for line in f:
        paths.append([(move[0], int(move[1:])) for move in line.rstrip().split(',')])

moves = {
    'U': (0, 1),
    'R': (1, 0),
    'D': (0, -1),
    'L': (-1, 0),
}

def get_points(path):
    result = {}
    pos = (0, 0)
    length = 0
    for move, count in path:
        for _ in range(count):
            pos = (pos[0] + moves[move][0], pos[1] + moves[move][1])
            length += 1
            if pos not in result:
                result[pos] = length
    return result

A = get_points(paths[0])
B = get_points(paths[1])

intersection = A.keys() & B.keys() # intersection

print(1, min(abs(x) + abs(y) for x, y in intersection))
print(2, min(A[p] + B[p] for p in intersection))
