import itertools

def run_intcode(opcodes):
    position = 0
    while opcodes[position] != 99:
        if opcodes[position] == 1:
            opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] + opcodes[opcodes[position + 2]]
        elif opcodes[position] == 2:
            opcodes[opcodes[position + 3]] = opcodes[opcodes[position + 1]] * opcodes[opcodes[position + 2]]
        else:
            # print('unknown opcode')
            return [0] # in this case [0] is not a valid result... but we should trigger an error
        position += 4
    return opcodes

with open('input.txt', 'r') as f:
    values = [int(i) for i in f.readline().rstrip().split(',')]

# print(values)

def first():
    if run_intcode([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]) != [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]:
        print('fail')
    if run_intcode([1, 0, 0, 0, 99]) != [2, 0, 0, 0, 99]:
        print('fail')
    
    values[1] = 12
    values[2] = 2
    print(run_intcode(values)[0])

def second():
    for noun, verb in list(itertools.product(range(100), repeat=2)):
        values[1] = noun
        values[2] = verb
        if run_intcode(values.copy())[0] == 19690720:
            print(100 * noun + verb)
            break

if __name__ == "__main__":
    # first()
    second()
