def get_fuel(mass):
    return mass // 3 - 2

def get_fuel_2(mass):
    fuel = mass // 3 - 2
    if fuel <= 0:
        return 0
    return fuel + get_fuel_2(fuel)

# with open('input.txt', 'r') as f:
#     input = [int(l.rstrip()) for l in f]

def first():
    # for m, f in [(12, 2), (14, 2), (1969, 654), (100756, 33583)]:
    #     if get_fuel(m) != f : print(str(m) + ' is not ' + str(f))

    fuel = 0
    with open('input.txt', 'r') as f:
        for l in f:
            fuel += get_fuel(int(l.rstrip()))

    print(fuel)

def second():
    # for m, f in [(14, 2), (1969, 966), (100756, 50346)]:
    #     f2 = get_fuel_2(m)
    #     if f2 != f : print(str(m) + ' is not ' + str(f) + ' it is ' + str(f2))

    fuel = 0
    with open('input.txt', 'r') as f:
        for l in f:
            fuel += get_fuel_2(int(l.rstrip()))

    print(fuel)

    # 10271227

if __name__ == "__main__":
    # first()
    second()
