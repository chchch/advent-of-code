with open('input.txt', 'r') as f:
    values = [a.rstrip().split(')') for a in f]
# print(values)

def first(planets):

    def count_orbiting(planet, carry_over):
        result = carry_over
        for p in planets.get(planet, []):
            result += count_orbiting(p, carry_over + 1)
        return result

    print(count_orbiting('COM', 0))

def second(planets):

    paths = {}
    def parse_orbiting(planet, path):
        orbiting = planets.get(planet, [])
        if not orbiting:
            paths[planet] = path
        else:
            for p in orbiting:
                parse_orbiting(p, path.union(set([planet])))

    parse_orbiting('COM', set())

    print(len(paths['YOU'].symmetric_difference(paths['SAN'])))

if __name__ == "__main__":

    planets = {}
    for planet in values:
        planets.setdefault(planet[0], []).append(planet[1])
    # print(planets)
    # first(planets)
    second(planets)
