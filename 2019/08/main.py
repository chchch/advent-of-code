from itertools import islice
def first():
    values = []
    image_size = 25 * 6
    with open('input.txt', 'r') as f:
        data = (f.read(image_size))
        while data:
            values.append(data)
            data = str(f.read(image_size)).rstrip()

    min_zero = (len(values[0]), values[0])
    for layer in values:
        count_zero = layer.count('0')
        if count_zero < min_zero[0]:
            min_zero = (count_zero, layer)

    print(min_zero[1].count('1') * min_zero[1].count('2'))

def second():
    values = []
    image_size = 25 * 6
    with open('input.txt', 'r') as f:
        data = (f.read(image_size))
        while data:
            values.append(data)
            data = str(f.read(image_size)).rstrip()

    # 0 is black, 1 is white, and 2 is transparent

    result = [int(i) for i in values.pop(0)]

    for layer in values:
        for i in range(len(layer)):
          if result[i] == 2:
              result[i] = int(layer[i])

    output = ''
    for i in range(6):
        for j in range(25):
            c = str(result[i * 25 + j])
            output += ' ' if c == '0' else 'x'
        output += '\n'
    print(output)

if __name__ == '__main__':
    # first()
    second()
