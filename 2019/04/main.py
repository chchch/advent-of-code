range_from = 172851
range_to = 675869

def get_digits(n):
    result = []
    while n > 0:
        result.insert(0, n % 10)
        n //= 10
    return result

def is_matching(digits):
    has_double = False
    a = digits.pop(0)
    for i in digits:
        if i < a:
            return False
        if i == a:
            has_double = True
        a = i
    return has_double

def is_matching_2(digits):
    has_double = False
    double_n = 0
    a = digits.pop(0)
    for i in digits:
        if i < a:
            return False
        if i == a:
            double_n += 1
        else:
            if double_n == 1:
                has_double = True
            double_n = 0
        a = i

    if double_n == 1:
        has_double = True
            
    return has_double


def main():
    matching = 0
    for i in range(range_from, range_to + 1):
        if is_matching_2(get_digits(i)):
            matching += 1
    print(matching)
    # 1660
    # 1135

if __name__ == "__main__":
    main()
