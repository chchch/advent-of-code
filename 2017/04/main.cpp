#include<string>
#include<vector>
#include<set>
#include<algorithm>

#include <fstream>
#include <sstream>
#include <iterator>

#include<iostream>
#include<cassert>

bool hasUniqueWords(std::string line)
{
    std::istringstream iss{line};
    std::vector<std::string> words{std::istream_iterator<std::string>{iss}, {}};

    std::sort(words.begin(), words.end());
    auto last = std::unique(words.begin(), words.end());
    
    return last == words.end();
}

bool hasAnagram(std::string line)
{
    std::istringstream iss{line};
    std::vector<std::string> words{std::istream_iterator<std::string>{iss}, {}};

    std::transform(words.begin(), words.end(), words.begin(),
        [](auto w) {std::sort(w.begin(), w.end()); return w;}
    );

    std::sort(words.begin(), words.end());
    auto last = std::unique(words.begin(), words.end());
    
    return last != words.end();
}

/**
 * from the reddit thread
 */
bool hasAnagram2(std::string line)
{
    std::istringstream iss{line};
    std::set<std::string> words;
    for (std::string word; iss >> word;) {
        std::sort(word.begin(), word.end());
        if (!words.insert(word).second) {
            return true;
        }
    }
}

/**
 * from the reddit thread
 */
bool hasAnagram3(std::string line)
{
    std::istringstream iss{line};
    std::set<std::string> words;
    return !std::all_of(
        std::istream_iterator<std::string>{iss},
        {},
        [&] (auto word) {
            std::sort(word.begin(), word.end());
            return words.insert(word).second;
        }
    );
}

int main()
{

    assert(hasUniqueWords("aa bb cc dd ee") == true);
    assert(hasUniqueWords("aa bb cc dd aa") == false);
    assert(hasUniqueWords("aa bb cc dd aaa") == true);

    assert(hasAnagram2("abcde fghij") == false);
    assert(hasAnagram2("abcde xyz ecdab") == true);
    assert(hasAnagram2("a ab abc abd abf abj") == false);
    assert(hasAnagram2("iiii oiii ooii oooi oooo") == false);
    assert(hasAnagram2("oiii ioii iioi iiio") == true);

    assert(hasAnagram3("oiii ioii iioi iiio") == true);

    std::ifstream infile{"input.txt"};
    int countUnique{0};
    int countNoAnagram{0};
    for (std::string line; std::getline(infile, line);) {
        if (hasUniqueWords(line)) {
            countUnique++;
        }
        if (hasAnagram(line)) {
            countNoAnagram++;
        }
    }
    std::cout << countUnique << std::endl;
    std::cout << countNoAnagram << std::endl;
}
