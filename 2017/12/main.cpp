#include <fstream>
#include <sstream>
#include <iterator>

#include <vector>
#include <map>
#include <string>
#include <set>

#include <algorithm> // remove_if

#include<iostream>
#include<cassert>

using Node = std::map<int, std::vector<int>>;
using Connection = std::set<int>;

Node parse(std::string filename)
{
    Node node;
    std::ifstream infile{filename};
    for (std::string line; std::getline(infile, line);) {
        line.erase(remove_if(line.begin(), line.end(), [](int i)->bool {return i == ',';}), line.end());
        // 2 <-> 0, 3, 4
        int base;
        std::string dummy;
        std::stringstream ssl{line};
        ssl >> base >> dummy;
        std::vector<int> connection;
        for (int reference; ssl >> reference;) {
            if (reference != base) {
                connection.push_back(reference);
            }
        }
        node.insert({base, connection});
    }
    return node;
}

Connection getConnectedTo(Node node, Connection connection, int start)
{
    Connection result{start};
    result.merge(connection);
    for (auto i: node.at(start)) {
        if (result.find(i) == result.end()) {
            result.merge(getConnectedTo(node, result, i));
        }
    }
    return result;
}

using Group = std::map<int, Connection>;

int getGroupCount(Node node)
{
    Group group;
    for (auto n: node) {
        bool found{false};
        for (auto g: group) {
            found |= (std::find(g.second.begin(), g.second.end(), n.first) != g.second.end());
            if (found) break;
        }
        if (!found) {
            group.insert({n.first, getConnectedTo(node,{}, n.first)});
        }
    }
    return group.size();
}


int main()
{
    {
        Node node = parse("input-test.txt");
        assert(getConnectedTo(node, {}, 0).size() == 6);
        assert(getGroupCount(node) == 2);
    }
    {
        Node node = parse("input.txt");
        std::cout << getConnectedTo(node, {}, 0).size() << std::endl;
        std::cout << getGroupCount(node) << std::endl;
    }
}
