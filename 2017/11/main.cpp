#include <vector>
#include <string>
#include <utility>

#include <cmath>

#include <clocale> // isspace
#include <functional> // not1

#include <fstream>
#include <iterator>

#include <iostream>
#include <cassert>

struct Position
{
    int x{0}, y{0};

    /*
    operator std::pair<int,int>() const {
        return std::make_pair(x,y);
    }
    */

    bool operator==(const Position& b)
    {
        return x == b.x && y == b.y;
    }

    Position& operator+=(Position const& p)
    {
        x += p.x;
        y += p.y;
        return *this;
    }

    Position operator+(Position const& p) const
    {
        return Position{*this} += p;
    }
};

struct Grid {
    /**
     * inspired by https://stackoverflow.com/questions/5084801/manhattan-distance-between-tiles-in-a-hexagonal-grid
     */
    Position move(Position start, std::string direction) {
        Position movement{0,0};
        if (direction == "n") {
            movement.y = 1;
        } else if (direction == "ne") {
            movement.x = 1;
        } else if (direction == "se") {
            movement.x = 1;
            movement.y = -1;
        } else if (direction == "s") {
            movement.y = -1;
        } else if (direction == "sw") {
            movement.x = -1;
        } else if (direction == "nw") {
            movement.x = -1;
            movement.y = 1;
        }
        return start + movement;
    }

    void move(std::vector<std::string> path)
    {
        for (auto step: path) {
            position = move(position, step);
			maxDistance = std::max(maxDistance, getDistance());
        }
    }

    int getDistance()
    {
        int distance{0};
        int dx = position.x - 0;
        int dy = position.y - 0;

        if (dx *dy >= 0) { // same sign
            distance = std::abs(dx) + std::abs(dy);
        } else {
            distance = std::max(std::abs(dx), std::abs(dy));
        }
        // std::cout << "distance " << distance << std::endl;
        return distance;
    }

    Position position{0,0};

	int maxDistance{0};
};

int main()
{
    {
        Grid grid;
        grid.move({"ne", "ne", "ne"});
        assert((grid.position == Position{3,0}));
        assert(grid.getDistance() == 3);
    }
    {
        Grid grid;
        grid.move({"ne","ne","sw","sw"});
        assert((grid.position == Position{0,0}));
        assert(grid.getDistance() == 0);
    }
    {
        Grid grid;
        grid.move({"ne","ne","s","s"});
        assert((grid.position == Position{2,-2}));
        assert(grid.getDistance() == 2);
    }
    {
        Grid grid;
        grid.move({"se","sw","se","sw","sw"});
        assert((grid.position == Position{-1,-2}));
        assert(grid.getDistance() == 3);
    }
    {
        std::ifstream infile{"input.txt"};
        std::vector<std::string> input;
        for (std::string field; std::getline(infile, field, ',');) {
            field.erase(std::find_if(field.rbegin(), field.rend(), [](int ch) {return !std::isspace(ch);}).base(), field.end());
            input.push_back(field);
        }
        for (auto s: input) {
            std::cout << s << ".";
        }
        std::cout << std::endl;
        Grid grid;
        grid.move(input);
        std::cout << grid.getDistance() << std::endl;
        std::cout << grid.maxDistance << std::endl;
    }
}
