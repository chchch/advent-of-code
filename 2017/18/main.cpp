#include <vector>
#include <string>
#include <tuple>
#include <map>
#include <vector>

#include <cctype>
#include <algorithm>

#include <fstream>
#include <sstream>

#include <iostream>
#include <cassert>

using Program = std::vector<std::tuple<std::string, char, std::string>>;

Program getProgram(std::string filename)
{
    Program program;
    std::ifstream infile{filename};
    std::string command;
    char address;
    std::string value;
    for(std::string line; std::getline(infile, line); ) {
        std::stringstream ss{line};
        ss >> command;
        ss >> address;
        if (ss.rdbuf()->in_avail()) {
            ss >> value;
        } else {
            value = "";
        }
        program.push_back({command, address, value});
    }
    return program;
}

bool isInt(std::string value)
{
    return std::all_of(value.begin(), value.end(), [](char v) {return isdigit(v) || v == '-';});
}


int getFrequency(Program program) {
    std::map<char, long> addresses{};
    std::string command, value;
    char address;
    long valueInt{0};
    int i = 0;
    long lastSound{0};

    while (true) {
        auto instruction = program.at(i);
        command = std::get<0>(instruction);
        address = std::get<1>(instruction);
        value = std::get<2>(instruction);
        if (value != "") {
            if (isInt(value)) {
                valueInt = std::stoi(value);
            } else {
                valueInt = addresses[value.at(0)];
            }
        }

        if (command == "set") {
            addresses.insert_or_assign(address, valueInt);
        } else if (command == "add") {
            addresses[address] += valueInt;
        } else if (command == "mul") {
            addresses[address] *= valueInt;
        } else if (command == "mod") {
            addresses[address] %= valueInt;
        } else if (command == "snd") {
            lastSound = addresses[address];
        } else if (command == "rcv") {
            long v = addresses[address];
            if (v != 0) {
                addresses[address] = lastSound;
                std::cout << command << " " << address << " " << value << std::endl;
                std::cout << "played " << lastSound << std::endl;
                return lastSound;
            }
        }

        std::cout << i << ". " << addresses['a'] << std::endl;

        if ((command == "jgz") && (addresses[address] != 0)) {
            i += valueInt;
        } else {
            i++;
        }
    }
    return 0;
}

/*
struct Thread
{
    std::map<char, long> addresses{};
    std::array<std::vector<int>,2> buffer; 
};
*/

int getIntValue(std::string value, std::map<char, long> addresses) {
    int result;

    if (value != "") {
        if (isInt(value)) {
            result = std::stoi(value);
        } else {
            result = addresses[value.at(0)];
        }
    }
    return result;
}

using Thread = std::map<char, long>;

int getSentValues(Program program)
{
    std::vector<Thread> threads{{{'p', 0}},{{'p', 1}}};
    std::string command, value;
    char address;
    long valueInt{0};
    long lastSound{0};

    std::cout << threads[1]['p'] << std::endl;

    /*
    int i{0};
    while (true) {
        auto instruction = program.at(i);
        command = std::get<0>(instruction);
        address = std::get<1>(instruction);
        value = std::get<2>(instruction);
        valueInt = getIntValue(value, addresses);

        if (command == "snd") {
            int sendingValue = getIntValue(address, addresses);
        } else if (command == "rcv") {
        }
    }
    */

    return 0;
}

int main()
{
    {
        Program program = getProgram("input-test.txt");
        assert(getFrequency(program) == 4);
    }
    {
        Program program = getProgram("input.txt");
        std::cout << getFrequency(program) << std::endl;;
    }
    {
        Program program = getProgram("input-test-2.txt");
        assert(getSentValues(program) == 0);
    }
}
