#include <iostream>
#include <cassert>

#include <fstream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <limits>

#include <vector>
#include <utility>

#include <map>
#include <string>
#include <tuple>

using LayerItem = std::tuple<int, int, int, int>; // depth, range, current, direction
using Layer = std::map<int, LayerItem>;

Layer getLayer(std::string filename)
{
    Layer layer;
    std::ifstream infile{filename};
    for (std::string line; std::getline(infile, line);) {
        line.erase(remove_if(line.begin(), line.end(), [](int i)->bool {return i == ':';}), line.end());
        int depth{0}, range{0};
        std::stringstream(line) >> depth >> range;
        layer.insert({depth, {depth, range, 0, 1}});
    }
    return layer;
}

void increment(Layer& layer)
{
    std::for_each(layer.begin(), layer.end(), [](auto& l) {
        auto& ll = l.second;
        auto& c = std::get<2>(ll);
        c += std::get<3>(ll);
        if (c == 0 || c == std::get<1>(ll) - 1) {
            std::get<3>(ll) *= -1;
        }
    });
}

bool getCaught(Layer layer, int delay)
{
    // std::cout << "delay " << delay << std::endl;
    for (int i = 0; i < delay; i++) {
        increment(layer);
    }

    int n = layer.rbegin()->first;
    for (int i = 0; i < n + 1; i++) {
        LayerItem l{i, 0, 0, 0};
        try {
            l = layer.at(i);
        } catch (const std::exception& e) {
        }

        /*
        std::cout << "i " << i << ": ";
        for (auto l: layer) {
            std::cout << std::get<2>(l.second) << ".";
        }
        std::cout << std::endl;
        */

        // std::cout << "l " << std::get<2>(l) << std::endl;

        if (std::get<2>(l) == 0 && std::get<1>(l) > 0) { // if current == 0
            // std::cout << "caught" << std::endl;
            return true;
        }
        increment(layer);
    }
    // std::cout << "not caught" << std::endl;
    return false;
}

int getSeverity(Layer layer)
{
    int severity{0};

    int n = layer.rbegin()->first;
    for (int i = 0; i < n + 1; i++) {
        LayerItem l{i, 0, 0, 0};
        try {
            l = layer.at(i);
        } catch (const std::exception& e) {
        }

        std::cout << "i " << i << ": ";
        for (auto l: layer) {
            std::cout << std::get<2>(l.second) << ".";
        }
        std::cout << std::endl;

        std::cout << "l " << std::get<2>(l) << std::endl;

        if (std::get<2>(l) == 0) { // if current == 0
            std::cout << "<<<<" << std::endl;
            severity += i * std::get<1>(l); // depth * range
        }
        increment(layer);
    }
    std::cout << "severity " << severity << std::endl;;
    return severity;
}

int getDelay(Layer layer) {
    int i{0};
    while (getCaught(layer, i)) {
        // std::cout << "i " << i << std::endl;
        i++;
    }
    // std::cout << "i " << i << std::endl;
    return i;
}

int fromReddit()
{
    std::ifstream in ("input.txt");
    std::vector<std::pair<int, int>> scanners;
    std::string _;
    int depth, range;
    while (in >> depth >> _ >> range)
        scanners.emplace_back(depth, range);

    int delay = 0;
    while (!std::all_of(scanners.begin(), scanners.end(), [=](auto& p){ return (delay + p.first) % (2 * p.second - 2) != 0; }))
        ++delay;

    std::cout << delay << std::endl;
    return 0;
}

int main()
{
    /*
    {
        Layer layer = getLayer("input-test.txt");
        assert(getSeverity(layer) == 24);
    }
    {
        Layer layer = getLayer("input.txt");
        std::cout << getSeverity(layer) << std::endl;
    }
    */
	/*
    {
        Layer layer = getLayer("input-test.txt");
        assert(getDelay(layer) == 10);
    }
	*/
/*
    {
        Layer layer = getLayer("input.txt");
        std::cout << getDelay(layer) << std::endl;
    }
*/
	std::cout << fromReddit() << std::endl;
}
