#include <vector>
#include <numeric> // iota
#include <algorithm> // reverse

#include <fstream>
#include <iterator>

#include <iostream>
#include <cassert>

void render(std::vector<int> list) {
    for (int i: list) {
        std::cout << i << ".";
    }
    std::cout << std::endl;
}

int hash(int size, std::vector<int> input)
{
    std::vector<int> list(size);
    std::iota(list.begin(), list.end(), 0);

    int position{0};
    int skip{0};

    for (int move: input) {
        std::cout << "move " << move << std::endl;
        std::vector<int> buffer;
        int k = position;

        for (int j = 0; j < move; j++) {
            buffer.push_back(list.at(k));
            if (k < size - 1) {
                ++k;
            } else {
                k = 0;
            }
        }

        std::reverse(buffer.begin(), buffer.end());

        k = position;
        for (int v: buffer) {
            list.at(k) = v;
            if (k < size - 1) {
                ++k;
            } else {
                k = 0;
            }
        }

        /*
        render(list);
        if (move == 37) {
        break;
        }
        */

        position = (position + move + skip) % size;
        ++skip;
    }

    return list[0]*list[1];
}

int main()
{
    assert(hash(5, {3,4,1,5}) == 12);

    std::ifstream infile{"input.txt"};
    std::vector<int> input;
    for (std::string field; std::getline(infile, field, ',');) {
        input.push_back(std::stoi(field));
    }

    std::cout << hash(256, input) << std::endl;
}
