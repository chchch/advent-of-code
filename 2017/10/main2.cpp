#include <vector>
#include <numeric> // iota
#include <algorithm> // reverse, find_if
#include <iomanip> // setfill
#include <string>
#include <clocale> // isspace
#include <functional> // not1

#include <fstream>
#include <sstream>

#include <iostream>
#include <cassert>

void render(std::vector<int> list) {
    for (int i: list) {
        std::cout << i << ".";
    }
    std::cout << std::endl;
}

std::vector<int> toASCII(std::string hash)
{
    std::vector<int> ascii;
    for (char l: hash) {
        ascii.push_back(l);
    }
    return ascii;
}

std::string hash(int size, std::string input)
{
    std::vector<int> list(size);
    std::iota(list.begin(), list.end(), 0);

    auto ascii = toASCII(input);
    // render(ascii);
    auto asciiSuffix = {17, 31, 73, 47, 23};
    ascii.insert(ascii.end(), asciiSuffix.begin(), asciiSuffix.end());

    int position{0};
    int skip{0};

    for (int i = 0; i < 64; i++) {
        for (int move: ascii) {
            std::vector<int> buffer;
            int k = position;

            for (int j = 0; j < move; j++) {
                buffer.push_back(list.at(k));
                if (k < size - 1) {
                    ++k;
                } else {
                    k = 0;
                }
            }

            std::reverse(buffer.begin(), buffer.end());

            k = position;
            for (int v: buffer) {
                list.at(k) = v;
                if (k < size - 1) {
                    ++k;
                } else {
                    k = 0;
                }
            }

            position = (position + move + skip) % size;
            ++skip;
        }
    }

    // render(list);

    std::vector<int> denseHash;
    for (int i = 0; i < 16; i++) {
        denseHash.push_back(std::accumulate(list.begin() + (i * 16), list.begin() + ((i + 1) * 16),
            0,
            [](int a, int b) {
                return a ^ b;
            }
        ));

    }
    // render(denseHash);

    std::stringstream hexHash;
    for (int i: denseHash) {
        hexHash << std::setfill('0') << std::setw(2) << std::hex << i;
    }

    // std::cout << "hex " << hexHash.str() << std::endl;
    return hexHash.str();
}

int main()
{

    assert(hash(256, "") == "a2582a3a0e66e6e86e3812dcb672a272");
    assert(hash(256, "AoC 2017") == "33efeb34ea91902bb2f59c9920caa6cd");
    assert(hash(256, "1,2,3") == "3efbe78a8d82f29979031a4aa0b16a9d");
    assert(hash(256, "1,2,4") == "63960835bcdc130f0b66d7ff4f6a5a8e");

    std::ifstream infile{"input.txt"};
    std::stringstream ss;
    ss << infile.rdbuf();
    std::string input = ss.str();
    // input.erase(std::find_if(input.rbegin(), input.rend(), std::not1(std::isspace)).base(), input.end());
    // rtrim the string to get rid of the trailing \n
    input.erase(std::find_if(input.rbegin(), input.rend(), [](int ch) {return !std::isspace(ch);}).base(), input.end());
    std::cout << hash(256, input) << std::endl;
}
