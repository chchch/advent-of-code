#include <map>
#include <string>

#include <fstream>
#include <sstream>

#include <iostream>
#include <cassert>

class GroupsReader
{
    public:
        void next(char c) {
            if (inCancelled) {
                inCancelled = false;
                return ;
            }
            switch(c) {
                case '{' :
                    if (!inGarbage) {
                        inBracket = true;
                        bracketLevel++;
                        score += bracketLevel;
                    } else {
                        garbage++;
                    }
                break;
                case '}' :
                    if (!inGarbage) {
                        inBracket = false;
                        bracketLevel--;
                    } else {
                        garbage++;
                    }
                break;
                case '<' :
                    if (!inGarbage) {
                        inGarbage = true;
                    } else {
                        garbage++;
                    }
                break;
                case '>' :
                    inGarbage = false;
                break;
                case '!' :
                    inCancelled = true;
                break;
                default:
                    if (inGarbage) {
                        garbage++;
                    }
            }
        }
        int getScore() {
            if (bracketLevel == 0) {
                return score;
            } else {
                std::cout << "brackets error" << std::endl;
            }
            return -1;
        }
        int getGarbage() {
            return garbage;
        }
    private:
            bool inBracket{false};
            bool inGarbage{false};
            bool inCancelled{false};
            int bracketLevel{0};
            int score{0};
            int garbage{0};
};

int countGroups(std::string stream)
{
    GroupsReader reader;

    for (char c: stream) {
        reader.next(c);
    }

    return reader.getScore();
}

int countGarbage(std::string stream)
{
    GroupsReader reader;

    for (char c: stream) {
        reader.next(c);
    }

    return reader.getGarbage();
}

int main()
{
    assert(countGroups("{}") == 1);
    assert(countGroups("{{{}}}") == 6);
    assert(countGroups("{{},{}}") == 5);
    assert(countGroups("{{{},{},{{}}}}") == 16);
    assert(countGroups("{<a>,<a>,<a>,<a>}") == 1);
    assert(countGroups("{<{},{},{{}}>}") == 1);
    assert(countGroups("{{<ab>},{<ab>},{<ab>},{<ab>}}") == 9);
    assert(countGroups("{{<a>},{<a>},{<a>},{<a>}}") == 9);
    assert(countGroups("{{<!!>},{<!!>},{<!!>},{<!!>}}") == 9);

    assert(countGroups("{{<!>},{<!>},{<!>},{<a>}}") == 3);
    assert(countGroups("{{<a!>},{<a!>},{<a!>},{<ab>}}") == 3);
    assert(countGroups("{<{o\"i!a,<{i<a>}") == 1);

    std::ifstream infile{"input.txt"};
    std::stringstream ss;
    ss << infile.rdbuf();
    std::string stream = ss.str();

    std::cout << countGroups(stream) << std::endl;

    assert(countGarbage("{<>}") == 0);
    assert(countGarbage("{<random characters>}") == 17);
    assert(countGarbage("{<<<<>}") == 3);
    assert(countGarbage("<{!>}>") == 2);
    assert(countGarbage("<!!>") == 0);
    assert(countGarbage("<!!!>>") == 0);
    assert(countGarbage("<{o\"i!a,<{i<a>") == 10);
    std::cout << countGarbage(stream) << std::endl;
}
