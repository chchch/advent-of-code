#include <vector>
#include <numeric>
#include <algorithm>

#include <fstream>
#include <sstream>
#include <iterator>

#include <cassert>
#include <iostream>

// g++ -std=c++17 -o spreadsheet  main.cpp && ./spreadsheet

using Spreadsheet = std::vector<std::vector<int>>;

int checksum(Spreadsheet spreadsheet)
{
    return std::accumulate(spreadsheet.begin(), spreadsheet.end(),
        0,
        [](int sum, auto row) {
            auto [min, max] = std::minmax_element(row.begin(), row.end());
            return sum + *max - *min;
        }
    );
}

int evenlyDivisble(Spreadsheet spreadsheet) {
    return std::accumulate(spreadsheet.begin(), spreadsheet.end(),
        0,
        [](int sum, auto row) {
            for (int cell: row) {
                for (int other: row) {
                    if (cell != other && cell % other == 0) {
                        return sum + cell / other;
                    }
                }
            }
            return sum;
        }
    );
}

Spreadsheet readValues(std::string filename)
{
    Spreadsheet spreadsheet;
    std::ifstream infile{filename};
    for (std::string line; std::getline(infile, line);) {
        std::istringstream iss{line};
        std::vector<int> cells{std::istream_iterator<int>{iss}, {}};
        spreadsheet.push_back(cells);
    }
    return spreadsheet;
}

int main()
{
    assert(checksum({{1,2},{1,3},{1,4}}) == 6);
    assert(checksum({{5,1,9,5},{7,5,3},{2,4,6,8}}) == 18);
    Spreadsheet spreadsheet = readValues("input.txt");
    std::cout << checksum(spreadsheet) << std::endl;

    assert(evenlyDivisble({{5,9,2,8},{9,4,7,3},{3,8,6,5}}) == 9);
    std::cout << evenlyDivisble(spreadsheet) << std::endl;
}
