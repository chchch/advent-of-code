#include <vector>

#include <fstream>
#include <iterator>

#include <iostream>
#include <cassert>

int escape(std::vector<int> path)
{
    int steps{0};
    int position{0};
    int n = path.size();
    while (0 <= position && position < n) {
        steps++;
        position += path.at(position)++;
    }
    return steps;
}

int escape3(std::vector<int> path)
{
    int steps{0};

    for (int i{0}; 0 <= i && i < path.size();) {
        steps++;
        if (path.at(i) < 3) {
            i += path.at(i)++;
        } else {
            i += path.at(i)--;
        }
    }

    return steps;
}

/**
 * from the reddit thread
 */
int escape3iterator(std::vector<int> path)
{
    using iterator = std::vector<int>::iterator;
    int steps{0};

    for (iterator i{path.begin()}; path.begin() <= i && i < path.end();) {
        ++steps;
        iterator j = i;
        i += *i;
        *j += (*j < 3) ? 1 : -1;
    }

    return steps;
}


int main()
{
    std::ifstream infile{"input.txt"};
    std::vector<int> path {std::istream_iterator<int>{infile}, {}};

    assert(escape({0, 3, 0, 1, -3}) == 5);
    std::cout << escape(path) << std::endl;

    assert(escape3({0, 3, 0, 1, -3}) == 10);
    std::cout << escape3(path) << std::endl;

    assert(escape3iterator({0, 3, 0, 1, -3}) == 10);
}
