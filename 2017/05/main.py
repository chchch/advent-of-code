def go(path):
    i = 0
    while 0 <= i and i < len(path):
        j = i
        i += path[i]
        path[j] += 1
        print(i)
    return i

assert(go([0, 3, 0, 1, -3]) == 5)
