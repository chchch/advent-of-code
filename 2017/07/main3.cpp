#include <vector>
#include <string>

#include<regex>

#include<algorithm>

#include <fstream>

#include <iostream>
#include <cassert>

using DataItem = std::tuple<std::string, int, std::vector<std::string>>; // key, weight, children
using Data = std::vector<DataItem>;

Data readValues(std::string filename)
{
    Data data;
    std::ifstream infile{filename};

	std::string pattern = "(\\w+)\\s*\\((\\d+)\\)(?:\\s*->\\s*(.+))?";
    std::regex regex(pattern);
    std::smatch match;
    for (std::string line; std::getline(infile, line);) {
        std::vector<std::string>parent, child;
        if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
			parent.push_back(match[1].str());
            std::string values = match[3].str();
			if (values != "") {

                std::regex sep(", ");
                std::sregex_token_iterator token(values.cbegin(), values.cend(), sep, -1);
                std::sregex_token_iterator end;
                for (; token != end; ++token) {
                    // std::cout << "token " << *token << std::endl;
                    child.push_back(*token);
                }
			}
        }
        // std::cout << match[2] << std::endl;
        data.push_back({match[1].str(), std::stoi(match[2].str()), child});

        // data.push_back(line);
    }

    return data;
}

DataItem getHead(Data data)
{
    std::sort(data.begin(), data.end(),
        [](DataItem a, DataItem b) {
            auto c = std::get<2>(b);
            return !(std::get<2>(a).empty()) && std::find(c.begin(), c.end(), std::get<0>(a)) == c.end();
        }
    );
    for (auto item: data) {
        std::cout << std::get<0>(item) << std::endl;
    }
    return data.front();
}

DataItem getItem(std::string name, Data data)
{
    for (auto child: data) {
        if (std::get<0>(child) == name) {
            return child;
        }
    }
}

int getWeight(DataItem item, Data data)
{
    int weight{std::get<1>(item)};

    for (auto childName: std::get<2>(item)) {
        auto child = getItem(childName, data);
        weight += getWeight(child, data);
    }

    return weight;
}

int getRebalancedWeight(Data data)
{
    auto head = getHead(data);
    for (auto item: std::get<2>(head)) {
        std::cout << item << std::endl;
        auto child = getItem(item, data);
        std::cout << "own weight " << std::get<1>(child) << std::endl;
        int weight = getWeight(child, data);
        std::cout << "weight total " << weight << std::endl;
    }
    return 0;
}


int main()
{
    Data dataTest = readValues("input-test.txt");
    // std::cout << std::get<0>(getHead(dataTest)) << std::endl;

    // std::cout << getRebalancedWeight(dataTest) << std::endl;

    Data data = readValues("input.txt");
    /*
    for (auto item: data) {
        std::cout << "- " << std::get<0>(item) << std::endl;
        for (auto child: std::get<2>(item)) {
            std::cout << " ." << child;
        }
        std::cout << std::endl;
    }
    */
    std::cout << "head " << std::get<0>(getHead(data)) << std::endl;
    std::cout << getRebalancedWeight(data) << std::endl;
}
