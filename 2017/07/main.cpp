#include<string>
#include<vector>
#include<regex>
#include <iterator>

#include <fstream>

#include<iostream>
#include<cassert>

using Data = std::pair<std::vector<std::string>, std::vector<std::string>>;

std::string getRoot(Data data)
{
    for (auto child: data.second) {
        data.first.erase(std::find(data.first.begin(), data.first.end(), child));
    }
    return data.first.at(0);
}

Data readValues(std::string filename)
{
    std::ifstream infile{filename};
    std::vector<std::string>parent, child;

	std::string pattern = "(\\w+) \\(\\d+\\)(?:\\s*->\\s*(.+))?";
    std::regex regex(pattern);
    std::smatch match;
    for (std::string line; std::getline(infile, line);) {
        if (std::regex_match(line, match, regex, std::regex_constants::match_any)) {
			parent.push_back(match[1].str());
			if (match[2] != "") {
				std::string values = match[2].str();

                std::regex sep(", ");
                std::sregex_token_iterator token(values.cbegin(), values.cend(), sep, -1);
                std::sregex_token_iterator end;
                for (; token != end; ++token) {
                    child.push_back(*token);
                }
			}
        }

        // data.push_back(line);
    }

    return std::make_pair(parent, child);
}

int main() {
    // std::ifstream infile{filename};
    // std::vector<std::string> data{std::istream_iterator<std::string>{infile}, {}};
    Data dataTest = readValues("input-test.txt");
    assert(getRoot(dataTest) == "tknk");
    Data data = readValues("input.txt");
    std::cout << getRoot(data) << std::endl;
}
