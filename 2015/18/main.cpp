#include <iostream>
#include <fstream>
#include <string>

#include <array>

#include <cassert>

// using Grid = std::array<std::array<char, 6>, 6>;
using Grid = std::array<std::array<char, 100>, 100>;

int count_neighbors(const Grid& grid, int x, int y)
{
    int count{0};
    for (int j = - 1; j <= 1; j++) {
        int y_j = y + j;
        if (y_j < 0 || y_j > grid.size() - 1) {
            continue;
        }
        for (int i = - 1; i <= 1; i++) {
            if (i == 0 && j == 0) {
                continue;
            }
            int x_i = x + i;
            if (x_i < 0 || x_i > grid.at(y_j).size() - 1) {
                continue;
            }
            // std::cout << x_i << ",  " << y_j << std::endl;
            if (grid.at(y_j).at(x_i) == '#') {
                count++;
            }
        }
    }
    return count;
}

void turn_on_corners(Grid& grid) {
    grid.at(0).at(0) = '#';
    grid.at(0).at(grid.at(0).size() - 1) = '#';
    grid.at(grid.size() - 1).at(grid.at(0).size() - 1) = '#';
    grid.at(grid.size() - 1).at(0) = '#';
}

int main()
{
    bool part_2{true};
    Grid grid;
    // std::ifstream input("input-test.txt");
    std::ifstream input("input.txt");
    int j = 0;
    for (std::string line; getline(input, line);) {
        for (int i = 0; i < line.size(); i++) {
            grid.at(j).at(i) = line.at(i);
        }
        j++;
    }

    // assert(count_neighbors(grid, 1, 0) == 0);
    // assert(count_neighbors(grid, 3, 0) == 2);

    if (part_2) {
        turn_on_corners(grid);
    }

    const int n_steps{100};
    // const int n_steps{5};
    for (int step = 0; step < n_steps; step++) {
        Grid next{};
        for (int j = 0; j < grid.size(); j++) {
            for (int i = 0; i < grid.at(j).size(); i++) {
                int neighbors = count_neighbors(grid, i, j);
                if (grid.at(j).at(i) == '#') {
                    next.at(j).at(i) = neighbors == 2 || neighbors == 3 ? '#' : '.';
                } else {
                    next.at(j).at(i) = neighbors == 3 ? '#' : '.';
                }
            }
        }
        grid = next;
        if (part_2) {
            turn_on_corners(grid);
        }
    }
    int count{0};
    for (int j = 0; j < grid.size(); j++) {
        for (int i = 0; i < grid.at(j).size(); i++) {
            std::cout << grid.at(j).at(i);
            if (grid.at(j).at(i) == '#') {
                count++;
            }
        }
        std::cout << std::endl;
    }
    std::cout << count << std::endl;
}
