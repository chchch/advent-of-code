#include <iostream>

int main()
{
    std::string input{"1113122113"};
    // std::string input{"1"};
    // int n{5};
    long long n{50};

    for (int i = 0; i < n; i++) {
        std::string next{};
        char prev{input[0]};
        int count = 1;
        input = input.substr(1);
        for (const auto& c: input) {
            if (c == prev) {
                count++;
            } else {
                next += std::to_string(count) + prev;
                prev = c;
                count = 1;
            }
        }
        next += std::to_string(count) + prev;
        input = next;
        // std::cout << input << std::endl;
    }
    std::cout << input.size() << std::endl;
}
