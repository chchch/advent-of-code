#include <iostream>
#include <cmath>
#include <vector>

int main()
{
    bool part_1{false};
    long long input = 33100000;
    int house{0};
    long long total{0};
    while (total < input) {
        total = 0;
        house++;
        int n = (int) sqrt((double) house) + 1;
        for (int i = 1; i <= n; i++) {
            if (house % i == 0) {
                if (part_1)
                {
                    total += i * 10;
                    if (house != i * i) {
                        total += house / i * 10;
                    }
                } else {
                    if (house / i <= 50) {
                        total += i * 11;
                    }
                    if (i <= 50) { // h / (h / i)
                        if (house != i * i) {
                            total += house / i * 11;
                        }
                    }
                }
            }
        }
        // std::cout << house << " " << total << std::endl;
        if (total >= input) {
            std::cout << house << std::endl;
            break;
        }
    }
}
