#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <vector>
#include <string>
#include <algorithm>

struct Ingredient
{
    int capacity;
    int durability;
    int flavor;
    int texture;
    int calories;
};

int main()
{
    std::vector<Ingredient> ingredients{};
    // std::ifstream input("input-test.txt");
    std::ifstream input("input.txt");
    for (std::string line; getline(input, line);) {
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
        Ingredient ingredient{
            std::stoi(tokens[2]), std::stoi(tokens[4]),
            std::stoi(tokens[6]), std::stoi(tokens[8]),
            std::stoi(tokens[10])};
        ingredients.push_back(ingredient);
    }

    long long max_score{0};
    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 100 - i; j++) {
            for (int k = 0; k < 100 - i - j; k++) {
                std::vector<long long> ratio{i, j, k, 100 - i -j - k};
                long long capacity{0}; long long durability{0};
                long long flavor{0};
                long long texture{0};
                long long calories{0};
                for (int r = 0; r < ratio.size(); r++) {
                    capacity += ratio.at(r) * ingredients.at(r).capacity;
                    durability += ratio.at(r) * ingredients.at(r).durability;
                    flavor += ratio.at(r) * ingredients.at(r).flavor;
                    texture += ratio.at(r) * ingredients.at(r).texture;
                    calories += ratio.at(r) * ingredients.at(r).calories;
                }
                if (capacity <= 0 || durability <= 0
                ||  flavor <= 0 || texture <= 0) {
                    continue;
                }
                if (calories != 500) {
                    continue;
                }
                max_score = std::max(max_score, capacity * durability * flavor * texture);
            }
        }
    }
    std::cout << max_score << std::endl;
}
