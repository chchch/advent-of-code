#include <fstream>
#include <string>
#include <iostream>
#include <unordered_map>

struct Point {
    int x{0};
    int y{0};
    Point operator+ (const Point &other) const {
        return Point{x + other.x, y + other.y};
    }
    Point& operator+=(const Point& other){
        this->x += other.x;
        this->y += other.y;
        return *this;
    }
    bool operator==(const Point& other) const {
        return x == other.x && y == other.y;
    }
};

// https://stackoverflow.com/a/7222201/5239250
template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

// https://stackoverflow.com/a/43933806/5239250
namespace std
{
template<>
class hash<Point>
{
public:
    size_t operator()(Point const& point) const
    {
        size_t seed = 0;
        ::hash_combine(seed, point.x);
        ::hash_combine(seed, point.y);
        return seed;
    }
};
}

int main()
{
    std::unordered_map<char, Point> movements {
        {'>', Point{1, 0}},
        {'<', Point{-1, 0}},
        {'^', Point{0, 1}},
        {'v', Point{0, -1}},
    };

    std::unordered_map<Point, int> grid;

    std::ifstream in ("input.txt");
    std::string directions(std::istreambuf_iterator<char>{in}, {});

    Point position{};
    grid[position]++;
    for (const auto& c: directions) {
        position += movements.at(c);
        grid[position]++;
    }

    std::cout << grid.size() << std::endl;

    grid.clear();
    Point santa{};
    Point robo_santa{};
    grid[santa]++;
    grid[robo_santa]++;
    bool santa_moves{true};
    for (const auto& c: directions) {
        if (santa_moves) {
            santa += movements.at(c);
            grid[santa]++;
        } else {
            robo_santa += movements.at(c);
            grid[robo_santa]++;
        }
        santa_moves = !santa_moves;
    }
    std::cout << grid.size() << std::endl;
}
