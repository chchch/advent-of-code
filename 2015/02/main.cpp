#include<fstream>
#include <iostream>
#include <string>
#include<sstream>
#include <vector>
#include <algorithm>
#include <numeric>

// https://stackoverflow.com/a/27511119/5239250
std::vector<int> get_box(const std::string &s, char delim) {
  std::stringstream ss(s);
  std::string item;
  std::vector<int> result;
  while (std::getline(ss, item, delim)) {
    result.push_back(std::stoi(item));
  }
  return result;
}

int main()
{
    std::ifstream input("input.txt");
    std::vector<int> surfaces{};
    long long paper{0};
    long long ribbon{0};
    // std::vector<std::string> lines{"2x3x4"};
    // for(std::string line: lines) {
    for(std::string line; getline( input, line );) {
        surfaces.clear();
        auto box = get_box(line, 'x');
        surfaces.push_back(box.at(0) * box.at(1));
        surfaces.push_back(box.at(1) * box.at(2));
        surfaces.push_back(box.at(0) * box.at(2));
        int extra = *std::min_element(surfaces.begin(), surfaces.end());
        paper += extra + std::accumulate(surfaces.begin(), surfaces.end(), 0,
            [](long long a, const auto& s) { return a + 2 * s; });

        auto max_element = std::max_element(box.begin(), box.end());
        std::iter_swap(max_element, box.end() - 1);
        ribbon += 2 * std::accumulate(box.begin(), box.end() - 1, 0) +
           std::accumulate(box.begin(), box.end(), 1, std::multiplies<long long>());
    }
    std::cout << paper << std::endl;
    std::cout << ribbon << std::endl;
}
