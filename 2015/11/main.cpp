#include <iostream>

std::string increment(std::string password)
{
    long unsigned n{password.size() - 1};
    while (n > 0 && password[n] == 'z') {
        password[n] = 'a';
        n--;
    }
    if (n >= 0) {
        password[n]++;
    }
    return password;
}

bool is_valid(const std::string& password)
{
    {
        bool found{false};
        for (int i = 0; i < password.size() - 2; i++) {
            if (password[i] == password[i + 1] - 1
            &&  password[i] == password[i + 2] - 2) {
                found = true;
            }
        }
        if (!found) {
            return false;
        }
    }
    if (password.find('i') != std::string::npos
    ||  password.find('o') != std::string::npos
    ||  password.find('l') != std::string::npos) {
        return false;
    }
    {
        int found = 0;
        for (int i = 0; i < password.size() - 1; i++) {
            if (password[i] == password[i + 1]) {
                found++;
                if (found == 2) {
                    return true;
                }
                i++; // non overlapping
            }
        }
    }
    return false;
}

std::string next(std::string password) {
    do {
        password = increment(password);
    } while (!is_valid(password));
    return password;
}

int main()
{
    std::cout << increment("b") << " > c" << std::endl;
    std::cout << increment("az") << " > ba" << std::endl;
    std::cout << increment("abc") << " > abd" << std::endl;

    std::cout << is_valid("hijklmmn") << " > false" << std::endl;
    std::cout << is_valid("abbceffg") << " > false" << std::endl;
    std::cout << is_valid("abbcegjk") << " > false" << std::endl;
    std::cout << is_valid("abcdffaa") << " > true" << std::endl;

    {
        std::string password{"abcdefgh"};
    }

    {
        std::string password{"abcdefgh"};
        // password = increment(password);
        // std::cout << password << std::endl;
        // std::cout << increment(password) << std::endl;
        std::cout << next(password) << std::endl;
    }
    {
        std::string password{"hxbxwxba"};
        std::cout << next(password) << std::endl;
    }
    {
        std::string password{"hxbxxyzz"};
        std::cout << next(password) << std::endl;
    }
}
