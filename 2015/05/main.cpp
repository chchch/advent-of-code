#include <iostream>
#include <string>
#include <fstream>

#include <set>

// It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
// It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
// It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.

bool is_nice(std::string string)
{
    int vowels = 0;
    bool repeated = false;
    char precedent{' '};
    for (const auto& c: string) {
        if (c == 'a' || c == 'e' || c == 'i'  || c == 'o'  || c == 'u') {
            vowels++;
        }
        if (precedent == 'a' && c == 'b' || precedent == 'c' && c == 'd'
         || precedent == 'p' && c == 'q' || precedent == 'x' && c == 'y') {
            return false;
        }
        if (precedent == c) {
            repeated = true;
        }
        precedent = c;
    }
    return vowels >= 3 && repeated;
}

// It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
// It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
//
bool is_nice_new(std::string string)
{
    std::set<std::string> pairs{};
    bool found_pair = false;
    bool found_repeat = false;
    char precedent{' '};
    char preprecedent{' '};
    for (const auto& c: string) {
        if (!found_pair) {
            found_pair = pairs.find(std::string{precedent, c}) != pairs.end();
            pairs.insert(std::string{preprecedent, precedent});
        }
        if (!found_repeat && c == preprecedent) {
            found_repeat = true;
        }

        if (found_pair  && found_repeat) {
            return true;
        }

        preprecedent = precedent;
        precedent = c;
    }
    return false;
}

int main()
{

    // std::cout << is_nice("ugknbfddgicrmopn") << std::endl;
    // std::cout << is_nice("aaa") << std::endl;
    // std::cout << is_nice("jchzalrnumimnmhp") << std::endl;
    // std::cout << is_nice("haegwjzuvuyypxyu") << std::endl;
    // std::cout << is_nice("dvszwmarrgswjxmb") << std::endl;

    // std::cout << is_nice_new("qjhvhtzxzqqjkmpb") << std::endl;
    // std::cout << is_nice_new("xxyxx") << std::endl;
    // std::cout << is_nice_new("uurcxstgmygtbstg") << std::endl;
    // std::cout << is_nice_new("ieodomkazucvgmuy") << std::endl;

    int i{0};
    int j{0};
    std::ifstream file("input.txt");
    std::string line;
    while (std::getline(file, line)) {
        if (is_nice(line)) {
            i++;
        }
        if (is_nice_new(line)) {
            j++;
        }
    }
    std::cout << i << std::endl;
    std::cout << j << std::endl;
}
