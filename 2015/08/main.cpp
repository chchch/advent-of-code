#include <iostream>
#include <vector>
#include <string>
#include <fstream>

std::string get_unescaped(const std::string& string)
{
    std::string result{};
    int hex_chars = 0;
    char prev_c{'\0'};
    for (const auto c: string) {
        if (c == '"') {
            if (prev_c == '\\') {
                result += c;
            }
            prev_c = c;
        } else if (c == 'x') {
            if (prev_c == '\\') {
                hex_chars = 1;
            } else {
                result += c;
            }
            prev_c = c;
        } else if (c == '\\') {
            if (prev_c == '\\') {
                result += '\\';
                prev_c = '\0';
            } else {
                prev_c = c;
            }
        } else if (hex_chars > 0) {
            // if (std::isdigit(c)) {
                if (hex_chars > 1) {
                    result += '\''; // we don't care about the real code...
                    hex_chars = 0;
                } else {
                    hex_chars++;
                }
            // } else {
            //     hex_chars = 0;
            //     result += "x" + prev_c + c;
            // }
            prev_c = c;
        } else {
            result += c;
            prev_c = c;
        }
    }
    return result;
}

std::string get_escaped(const std::string& string)
{
    std::string result{};
    for (const auto c: string) {
        if (c == '"') {
            result += "\\\"";
        } else if (c == '\\') {
            result += "\\\\";
        } else {
            result += c;
        }
    }
    return "\"" + result + "\"";
}

int main()
{
    std::vector<std::string> lines = {
        "\"\"",
        "\"abc\"",
        "\"aaa\\\"aaa\"",
        "\"\\x27\"",
    };

    int n{0};
    int m{0};
    int o{0};

    std::ifstream file("input.txt");
    std::string line;
    while (std::getline(file, line)) {
    // for (const auto& line: lines) {
        std::cout << line << " " << line.size() << std::endl;
        // std::cout << get_unescaped(line) << " " << get_unescaped(line).size() << std::endl;
        n += line.size();
        m += get_unescaped(line).size();
        std::cout << get_escaped(line) << " " << get_escaped(line).size() << std::endl;
        o += get_escaped(line).size();
    }
    std::cout << "n " << n << std::endl;
    std::cout << "m " << m << std::endl;
    std::cout << "n - m " << n - m << std::endl;
    std::cout << "o - n " << o - n << std::endl;

}
