#include <iostream>
#include <array>

// To continue, please consult the code grid in the manual.  Enter the code at row 2978, column 3083.

int main()
{
    long long code = 20151125;
    int i = 0;
    bool found = false;
    while (!found) {
        i++;
        for (int x = 1, y = i; x <= i; x++, y--) {
            // std::cout << "x " << x << " y " << y << std::endl;
            if (x == 3083 && y == 2978) {
                std::cout << "<<<<<" << code << std::endl;
                found = true;
                break;
            }
            code = code * 252533 % 33554393;
        }
    }
    // 1: 2650453
    // 2: i need more stars...
}
