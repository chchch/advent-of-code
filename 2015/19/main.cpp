#include <iostream>

#include <fstream>
#include <sstream>
#include <iterator>
#include <string>
#include <vector>

#include <utility>
#include <algorithm>

#include <unordered_set>

using Replacements = std::vector<std::pair<std::string, std::string>>;

void part_1(const Replacements& replacements, std::string& molecule) {
    std::unordered_set<std::string> new_molecules{};
    for (const auto& replacement: replacements) {
        std::string molecule_start{};
        std::vector<std::string> new_m{};
        size_t pos{0};
        std::string search{std::get<0>(replacement)};
        while ((pos = molecule.find(search, pos)) != std::string::npos) {
            std::string new_molecule{};
            if (pos > 0) {
                new_molecule += molecule.substr(0, pos);
            }
            new_molecule += std::get<1>(replacement);
            if (pos + search.size() < molecule.size()) {
                new_molecule += molecule.substr(pos + search.size());
            }

            new_molecules.insert(new_molecule);
            pos += search.size();
        }
    }

    for (const auto& m: new_molecules) {
        std::cout << m << std::endl;
    }
    std::cout << new_molecules.size() << std::endl;
}

// c++ implementation of the python conversion of the perl solution in reddit...
void part_2(const Replacements& replacements, const std::string& target) {

    int steps{0};
    std::string molecule{target};
    std::reverse(molecule.begin(), molecule.end());
    // std::cout << "molecule" << molecule << std::endl;
    Replacements i_replacements{};
    for (auto [key, value]: replacements) {
        // std::cout << "k, v " << key << " " << value << std::endl;
        std::reverse(key.begin(), key.end());
        std::reverse(value.begin(), value.end());
        i_replacements.push_back(std::make_pair(key, value));
    }
    while (molecule != "e") {
        bool found{false};
        size_t first_pos{molecule.size() + 1};
        std::string searched{};
        std::string replaced{};
        for (const auto& replacement: i_replacements) {
            size_t pos{0};
            std::string search{std::get<1>(replacement)};
            std::string replace{std::get<0>(replacement)};
            if ((pos = molecule.find(search, pos)) != std::string::npos) {
                found = true;
                if (pos < first_pos) {
                    first_pos = pos;
                    searched = search;
                    replaced = replace;
                }
            }
        }
        molecule = molecule.substr(0, first_pos)
            + replaced
            + molecule.substr(first_pos + searched.size());
        steps++;
    }
    std::cout << steps << std::endl;
}

int main()
{
    bool reading_replacements{true};
    std::string molecule{};
    Replacements replacements{};
    std::ifstream input("input.txt");
    // std::ifstream input("input-test.txt");
    // std::ifstream input("input-test-02.txt");
    for (std::string line; getline(input, line);) {
        if (line.empty()) {
            reading_replacements = false;
            continue;
        }
        if (reading_replacements) {
            std::istringstream iss(line);
            std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
            replacements.push_back(std::make_pair(tokens.at(0), tokens.at(2)));
        } else {
            molecule = line;
        }
    }

    // part_1(replacements, molecule);
    // molecule = "HOHOHO";
    part_2(replacements, molecule);

    // 715 too high
    // 603 too high
}
