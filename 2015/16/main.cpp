#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <functional>

int main()
{
    bool part_2 = true;
    std::unordered_map<std::string, int> aunts_things{
        {"children", 3},
        {"cats", 7},
        {"samoyeds", 2},
        {"pomeranians", 3},
        {"akitas", 0},
        {"vizslas", 0},
        {"goldfish", 5},
        {"trees", 3},
        {"cars", 2},
        {"perfumes", 1}};

    std::ifstream input("input.txt");
    for (std::string line; getline(input, line);) {
        line.erase(std::remove(line.begin(), line.end(), ':'), line.end());
        line.erase(std::remove(line.begin(), line.end(), ','), line.end());
        std::istringstream iss(line);
        std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, {}};
        // Sue 1: goldfish: 9, cars: 0, samoyeds: 9
        // Sue 500: cats: 2, goldfish: 9, children: 8
        bool found = true;
        for (int i = 2; i < tokens.size(); i += 2) {
            if (part_2) {
                auto compare = [](const auto c) {
                    if (c == "cats" || c == "tree") {
                        return std::function<bool(int, int)>([](int aunt_v, int v) { return v > aunt_v; });
                    } else if (c == "pomeranians" || c == "goldfish") {
                        return std::function<bool(int, int)>([](int aunt_v, int v) { return v < aunt_v; });
                    }
                    return std::function<bool(int, int)>([](int aunt_v, int v) { return v == aunt_v; });
                }(tokens.at(i));

                if (!compare(aunts_things[tokens.at(i)], std::stoi(tokens.at(i + 1)))) {
                    found = false;
                    break;
                }
                continue;
            }
            if (aunts_things[tokens.at(i)] != std::stoi(tokens.at(i + 1))) {
                found = false;
                break;
            }
        }
        if (found) {
            std::cout << tokens.at(1) << std::endl;
        }
    }
}
