#include<fstream>
#include <iostream>
#include <string>

int main()
{
    std::ifstream in ("input.txt");
    std::string directions(std::istreambuf_iterator<char>{in}, {});
    // std::string directions = "(()(()(";

    int step = 1;
    int position = 0;
    int floor = 0;
    for (const auto& c: directions) {
        if (c == '(') {
            floor++;
        } else if (c == ')') {
            floor--;
        }
        position++;
        if (step == 2 && floor == -1) {
            std::cout << position << std::endl;
            break;
        }
    }
    std::cout << floor << std::endl;;
}
