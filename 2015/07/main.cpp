#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include <optional>

std::vector<std::string> split(std::string string, std::string delimiter)
{
    size_t pos_start = 0, pos_end;
    std::string token;
    std::vector<std::string> tokens;

    while ((pos_end = string.find(delimiter, pos_start)) != std::string::npos) {
        token = string.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delimiter.size();
        tokens.push_back (token);
    }

    tokens.push_back (string.substr(pos_start));
    return tokens;
}

bool is_number(const std::string& s)
{
    return !s.empty() && std::find_if(s.begin(), 
            s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

struct Linked_node {
    Linked_node* next;
    std::string destination;
    std::vector<std::string> tokens{};
};

using Wires = std::unordered_map<std::string, int>;

// if the token is a number or is a defined field, return it as an int
std::optional<int> get_signal(const Wires& wires, std::string token)
{
    if (is_number(token)) {
        return std::stoi(token);
    } else if (wires.find(token) != wires.end()) {
        return wires.at(token);
    }
    return {};
}

int main()
{

    std::vector<std::string> lines {
        "123 -> x",
        "456 -> y",
        "x AND y -> d",
        "x OR y -> e",
        "x LSHIFT 2 -> f",
        "y RSHIFT 2 -> g",
        "NOT x -> h",
        "NOT y -> i"
    };

    Linked_node* first = nullptr;
    Linked_node* current = nullptr;

    std::ifstream file("input.txt");
    std::string line;
    while (std::getline(file, line)) {
    // for (const auto& line: lines) {
        auto tokens = split(line, " -> ");
        auto destination = tokens.at(1);
        tokens = split(tokens.at(0), " ");

        auto node = new Linked_node{first, destination, tokens};
        if (first == nullptr) {
            node->next = node;
            first = node;
        } else {
            current->next = node;
        }
        current = node;
    }

    auto preceding = current;
    current = first;
    Wires wires{};
    do {
        std::cout << current->destination << std::endl;
        bool wired = false;
        switch (current->tokens.size()) {
            case 1:
                // std::cout << tokens.at(0) << std::endl;;
                if (auto signal = get_signal(wires, current->tokens.at(0))) {
                    wires[current->destination] = *signal;
                    wired = true;
                }
            break;
            case 2:
                // it's always a NOT
                if (auto signal = get_signal(wires, current->tokens.at(1))) {
                    wires[current->destination] = 65535 - *signal;
                    wired = true;
                }
            break;
            case 3: {
                // std::cout << tokens.at(0) << " " << tokens.at(1) << " " << tokens.at(2) <<  std::endl;
                auto a = get_signal(wires, current->tokens.at(0));
                auto b = get_signal(wires, current->tokens.at(2));
                if (a && b) {
                    if (current->tokens.at(1) == "AND") {
                        wires[current->destination] = *a & *b;
                    } else if (current->tokens.at(1) == "OR") {
                        wires[current->destination] = *a | *b;
                    } else if (current->tokens.at(1) == "LSHIFT") {
                        wires[current->destination] = *a << *b;
                    } else if (current->tokens.at(1) == "RSHIFT") {
                        wires[current->destination] = *a >> *b;
                    } else {
                        std::cout << "unknown 3 token " << current->tokens.at(1) << std::endl;;
                    }
                    wired = true;
                }
                break;
            } 
            default:
                std::cout << "unknown number of tokens" << std::endl;;
            break;
        }
        std::cout << wired << std::endl;
        if (wired) {
            if (current->next != current) {
                auto processing = current;
                current = current->next;
                preceding->next = current;
                delete processing;
            } else {
                delete current;
                current = nullptr;
            }
        } else {
            preceding = current;
            current = current->next;
        }
    } while (current != nullptr);

    for (const auto& wire: wires) {
        // std::cout << wire.first << " >> " << wire.second << std::endl;
    }
    std::cout << wires["a"] << std::endl;
}
