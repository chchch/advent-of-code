#include <iostream>
#include <vector>
#include <numeric>


using List = std::vector<int>;
using Combinations = std::vector<List>;

Combinations get_combinations(List pool, int r)
{
    Combinations result{};
    List result_item{};
    auto n = pool.size();
    if (r > n)
        return {};

    std::vector<int> indices{};
    for (int i = 0; i < r; i++) {
        indices.push_back(i);
    }

    for (auto i: indices) {
        result_item.push_back(pool.at(i));
    }
    result.push_back(result_item);

    while (true) {
        bool found = false;
        int k = 0;
        for (int i = r - 1; i >= 0; i--) {
            if (indices[i] != i + n - r) {
                found = true;
                k = i;
                break;
            }

        }
        if (!found)
            break;

        indices.at(k)++;
        for (int j = k + 1; j < r; j++) {
            indices.at(j) = indices.at(j - 1) + 1;
        }

        result_item.clear();
        for (int i: indices) {
            result_item.push_back(pool.at(i));
        }
        result.push_back(result_item);
    }

    return result;
}

int main()
{
    std::vector<int> container_sizes = {11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3};

    int count{0};
    bool part_2{false};
    bool found{false};
    for (int r = 2; r < container_sizes.size(); r++) {
        for (const auto &c: get_combinations(container_sizes, r)) {
            if (std::accumulate(c.begin(), c.end(), 0) == 150) {
                count++;
                found = true;
            }
        }
        if (part_2 && found) {
            break;
        }
    }
    std::cout << count << std::endl;

}
