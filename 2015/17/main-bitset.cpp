#include <iostream>
#include <vector>
#include <bitset>

// https://www.fluentcpp.com/2019/12/17/how-to-increment-a-dynamic-bitset-with-the-stl/
void increment(std::bitset<20>& set)
{
    for (int i = 0; i < 20; i++) {
        set.flip(i);
        if (set[i] == 1) {
            break;
        }
    }
}
std::bitset<20> increment_ulong(std::bitset<20>& set)
{
    return std::bitset<20>(set.to_ulong() + 1);
}

int main()
{
    const std::vector<int> container_sizes = {11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3};
    std::bitset<20> combinations{};
    std::cout << combinations.to_string('o', '.') << std::endl;
    int count{0};
    while (!combinations.all()) {
        increment(combinations);
        int volume{0};
        for (int i = 0; i < 20; i++) {
            if (combinations.test(i)) {
                volume += container_sizes[i];
            }
        }
        if (volume == 150) {
            count++;
        }
    }
    std::cout << combinations.to_string('o', '.') << std::endl;
    // 1048575
    // 1048576
}
