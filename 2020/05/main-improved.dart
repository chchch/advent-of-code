import 'dart:io';
import 'dart:math';

main() {
  final part_1 = true;
  final path = true ? 'input.txt': 'input-demo.txt';

  final ids = File(path)
    .readAsLinesSync()
    // they're just binaries... thanks nic!
    .map((e) => e.split('').map((c) => c == 'F' || c == 'L' ? '0' : '1').join(''))
    .map((e) => int.parse(e, radix: 2));

  final max_id = ids.reduce(max);

  print(max_id);

  final min_id = max_id - ids.length;
  // https://math.stackexchange.com/questions/1842152/finding-the-sum-of-numbers-between-any-two-given-numbers
  final sum_bounds = (max_id - min_id + 1) * (min_id + max_id) ~/ 2;

  final sum_ids = ids.reduce((a, i) => a + i);

  print(sum_bounds - sum_ids);
    

}
