import 'dart:io';
import 'package:trotter/trotter.dart';
import 'dart:collection';
import 'dart:math';

main() {
  final demo = false;
  final path = 'input${demo ? '-demo' : ''}.txt';

  final data = File(path)
    .readAsLinesSync()
    .map((e) => int.parse(e))
    .toList();

  final preamble_n = demo ? 5 : 25;

  var invalid = 0;

  for (var i = preamble_n; i < data.length; i++) {
    if (!Combinations(2, data.sublist(i - preamble_n, i))()
      .any((e) => e[0] + e[1] == data[i])) {
        invalid = data[i];
        break;
    }
  }
  print('step 1: $invalid');

  var sample = ListQueue<int>();
  var sample_sum = 0;
  for (final num in data) {
    sample.add(num);
    sample_sum += num;
    while (sample_sum > invalid) {
      sample_sum -= sample.removeFirst();
    }
    if (sample_sum == invalid) {
      // sample.sort(); // ListQueue cannot be sorted
      // print('step 2: ${sample.first + sample.last}');
      print('step 2: ${sample.reduce(min) + sample.reduce(max)}');
      break;
    }
  }
}
