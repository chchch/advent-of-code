const demo = false;
const part_2 = true;
const numbers = demo ? [0, 3, 6] : [0, 3, 1, 6, 7, 5];

let memory = new Map(numbers
  .map((e, i) => [e, i + 1]));

const n = part_2 ? 30000000 : 2020;

let number_spoken = 0;
 
for (let i = numbers.length + 1; i < n; i++) {
  const next = memory.get(number_spoken);
  memory.set(number_spoken, i);
  number_spoken = next === undefined ? 0 : i - next;
}

console.log(`The ${n}th number spoken is ${number_spoken}`);
