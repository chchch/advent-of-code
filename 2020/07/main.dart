import 'dart:io';
import 'dart:collection';

class Bag {
  String color;
  var content = List();
  Bag(this.color);
  toString() {
    return '$color {$content}';
  }
}

void main() async {
  final part_1 = true;
  final path = false ? 'input.txt': 'input-demo${part_1 ? '' : '-2'}.txt';

  final pattern_row = RegExp(r'(\w+? \w+?) bags contain (.+).$');
  final pattern_contains = RegExp(r'(\d+) (\w+ \w+) bag[s]?');
	
  final rules_lines = await File(path)
    .readAsLinesSync()
    .map((e) => pattern_row.firstMatch(e).groups([1, 2]))
  ;

  var bags = Map<String, Bag>();
  var contained = Map<String, Set<String>>();
  for (final line in rules_lines) {
    var bag = Bag(line[0]);
    if (line[1] != 'no other bags') {
      for (final e in line[1].split(', ')) {
        final m = pattern_contains
          .firstMatch(e)
          .groups([1, 2]).toList();
        bag.content.add({'color': m[1], 'n': int.parse(m[0])});
        if (contained.containsKey(m[1])) {
          contained[m[1]].add(line[0]);
        } else {
          contained[m[1]] = Set.from([line[0]]);
        }
      }
    }
    bags[bag.color] = bag;
  }

  {
    var solution = Set<String>();
    var queue = ListQueue.from(['shiny gold']);
    while (queue.length > 0) {
      final item = queue.removeLast();
      if (!contained.containsKey(item)) {
        continue;
      }
      for (final container in contained[item]) {
        if (!solution.contains(container)) {
          solution.add(container);
          queue.add(container);
        }
      }
    }
    print('part 1: ${solution.length}');
  }

  {
    var solution = 0;
    var queue = ListQueue.from([{'color': 'shiny gold', 'n': 1}]);
    while (queue.length > 0) {
      final item = queue.removeLast();
      for (final bag in bags[item['color']].content) {
        solution += item['n'] * bag['n'];
        queue.add({'color': bag['color'], 'n': bag['n'] * item['n']});
      }
    }
    print('part 2: $solution');
  }
}
