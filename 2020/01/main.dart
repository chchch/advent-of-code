import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:trotter/trotter.dart';

void main() async {
  // https://stackoverflow.com/questions/21813401/reading-file-line-by-line-in-dart
  final part_1 = false;
  final path = false ? 'input.txt': 'input-demo.txt';

  final expenses = await File(path)
    .openRead()
    .map(utf8.decode)
    .transform(LineSplitter())
    .map(int.parse)
    .toList();
    // .forEach((l) => print('line: $l'));

  expenses.sort(); // if the data is not sorted it takes about 30 seconds

  if (part_1) {
      var combos = Combinations(2, expenses); // trotter
      // combos().forEach((c) => print(c));

      combos()
        .where((c) => c[0] + c[1] == 2020)
        .forEach((c) => print('solution: ${c[0] * c[1]}'));
  } else {
      print(Combinations(3, expenses)()
        .firstWhere((c) => c[0] + c[1] + c[2] == 2020)
        .reduce((a, i) => a * i));
  }

}
