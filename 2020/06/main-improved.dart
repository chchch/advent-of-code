// inspired by https://www.reddit.com/r/adventofcode/comments/k7ndux/2020_day_06_solutions/geul3zs
import 'dart:io';

void main() {
  final part_1 = false;
  final path = true ? 'input.txt': 'input-demo.txt';
  
  if (part_1) {
    final n = File(path)
      .readAsStringSync()
      .split('\n\n')
      .fold(0, (a, g) => a + Set.from(g.replaceAll('\n', '').split('')).length);
    print(n);
  } else {
    final n = File(path)
      .readAsStringSync()
      .split('\n\n')
      .fold(0, (a, g) => a + g
        .split('\n')
        .map((e) => Set.from(e.split('')))
        .fold(null, (b, i) => b == null ? i : b.intersection(i))
        .length
      );
    print(n);
  }
}
