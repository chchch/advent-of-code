import 'dart:async';
import 'dart:io';
import 'dart:convert';

void main() async {
  final part_1 = false;
  final path = true ? 'input.txt': 'input-demo.txt';

  final answer_lines = File(path)
    .readAsLinesSync();

  var n = 0;

  answer_lines.add(''); // get the last group to be calculated

  if (part_1) {
    var yes = Set();
    answer_lines.
      forEach((line) {
        if (line == '') {
          n += yes.length;
          yes.clear();
        } else {
          yes.addAll(line.split(''));
        }
      });
  } else {
    var yes = Map<String, int>();
    var n_persons = 0;
    answer_lines.
      forEach((line) {
        if (line == '') {
          n += yes
            .values
            .where((e) => (e == n_persons))
            .length;
          // print('$n_persons $yes $n');
          yes.clear();
          n_persons = 0;
        } else {
          line
            .split('')
            .forEach((c) => yes[c] = yes.containsKey(c) ? yes[c] + 1 : 1);
          n_persons++;
          // print(line);
        }
      });
  }
  print(n);
}
