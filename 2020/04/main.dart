import 'dart:async';
import 'dart:io';
import 'dart:convert';

extension Range on num {
  bool isIn(num from, num to) {
    return from <= this && this <= to;
  }
}

int is_valid(Map<String, String> passport) {
  return passport
    .map<String, bool>((k, v) => MapEntry(k, k == 'cid' || v != ''))
    .values
    .every((e) => e == true) ? 1 : 0;
}

final re_hcl = RegExp(r'^#[0-9a-f]{6}$');
final re_ecl = RegExp(r'^(amb|blu|brn|gry|grn|hzl|oth)$');
final re_pid = RegExp(r'^\d{9}$');

/*
byr (Birth Year) - four digits; at least 1920 and at most 2002.
iyr (Issue Year) - four digits; at least 2010 and at most 2020.
eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
hgt (Height) - a number followed by either cm or in:
    If cm, the number must be at least 150 and at most 193.
    If in, the number must be at least 59 and at most 76.
hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
pid (Passport ID) - a nine-digit number, including leading zeroes.
cid (Country ID) - ignored, missing or not.
*/
int is_valid_2(Map<String, String> passport) {
  return (int.tryParse(passport['byr'] ?? '')?.isIn(1920, 2002) ?? false)
    && (int.tryParse(passport['iyr'] ?? '')?.isIn(2010, 2020) ?? false)
    && (int.tryParse(passport['eyr'] ?? '')?.isIn(2020, 2030) ?? false)
    && (
      (passport['hgt']?.endsWith('cm') ?? false)
        && (int.tryParse(passport['hgt']?.substring(0, (passport['hgt']?.length ?? 2) - 2) ?? '')?.isIn(150, 193) ?? false)
      || (passport['hgt']?.endsWith('in') ?? false)
        && (int.tryParse(passport['hgt']?.substring(0, (passport['hgt']?.length ?? 2) - 2) ?? '')?.isIn(59, 76) ?? false)
    )
    && re_hcl.hasMatch(passport['hcl'] ?? '')
    && re_ecl.hasMatch(passport['ecl'] ?? '')
    && re_pid.hasMatch(passport['pid'] ?? '') ? 1 : 0;
}

void main() async {
  final part_1 = false;
  final path = true ? 'input.txt': 'input-demo${part_1 ? '' : '-2'}.txt';

  final batch_lines = File(path)
    .readAsLinesSync();

  var passport = {'byr': '', 'iyr': '', 'eyr': '', 'hgt': '',
    'hcl': '', 'ecl': '', 'pid': '', 'cid': ''};

  var n = 0;

  batch_lines.forEach((l) {
    if (l == '') {
      n += part_1 ? is_valid(passport) : is_valid_2(passport);
      passport.updateAll((k, v) => '');
      return;
    }
    l
      .split(' ')
      .forEach((e) {
        final k_v = e.split(':');
        passport[k_v[0]] = k_v[1];
      });
  });
  n += part_1 ? is_valid(passport) : is_valid_2(passport);

  print(n);
}
