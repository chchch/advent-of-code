# Advent of code 2020

## Dart

```
dart --enable-experiment=non-nullable main.dart
```

### Getting packages

- create a `pubspec.yaml` with `name` and `dependencies`
- `dart pub get`

### Reading files line by line

- <https://stackoverflow.com/questions/21813401/reading-file-line-by-line-in-dart>

### TOC

- 01: combinations, using packages from pubspec (dart + py).
- 02: regex or `isIn()` as extension to `int`.
- 03: _data classes_, `fold` vs. `reduce`.
- 04: parsing with regexp
- 05: binary search vs binary numbers (in improved) + sum 1..100 (dart + py).
- 06: improved: read groups by splitting by `\n\n`.
- 07: queue.
- 08: parse assembler commands.
- 09: combinations and queue.
- 10: fold with previous, current
- 11: clone list (game of life)
- 13: map and foreach with (e, i)
- 15: create a map from list entries
- 17: use Equatable for a hashable class (Coordinates, game of life 3D / 4D)
- 20: zip two lists
- 24: `set.contains()` needs an override for `hashCode` (hexagonal game of life)

### What about dart

What I dislike:

- `;` at the end of the line...
- "The right-hand side of an initializer does not have access to this." and that's a pity.
- Too often, the type deduction has surprising results (`print(v.fold(0, (a, e) => a + e));` will fail and one needs `print(v.fold<int>(0, (a, e) => a + e));`)
- No iterator destructuring / unpacking.
- Missing zip (iterating over two lists).

### Good solutions

- Day 4:
  - js: <https://www.reddit.com/r/adventofcode/comments/k6e8sw/2020_day_04_solutions/gelil1u>


### To be tested

Zipping?

```dart
Iterable<List<T>> zip<T>(Iterable<Iterable<T>> iterables) sync* {
  if (iterables.isEmpty) return;
  final iterators = iterables.map((e) => e.iterator).toList(growable: false);
  while (iterators.every((e) => e.moveNext())) {
    yield iterators.map((e) => e.current).toList(growable: false);
  }
}
```

### To be improved

#### Day 19

Look for good solutions without RegExp:

- https://github.com/adrbin/aoc/blob/main/2020/19/puzzle.js
- https://github.com/TamTran72111/aoc-2020/blob/main/python/day19.py
- https://github.com/ScottBailey/aoc-2020/blob/master/191/main.cpp (part 1... part 2 = 192)
- <https://en.wikipedia.org/wiki/CYK_algorithm>
- Python:

  ```py
	# given string s and list of rules seq is there a way to produce s using seq?
	def test(s,seq):
			if s == '' or seq == []:
					return s == '' and seq == [] # if both are empty, True. If only one, False.
			
			r = rules[seq[0]]
			if '"' in r:
					if s[0] in r:
							return test(s[1:], seq[1:]) # strip first character
					else:
							return False # wrong first character
			else:
					return any(test(s, t + seq[1:]) for t in r) # expand first term

	def parse_rule(s):
			n,e = s.split(": ")
			if '"' not in e:
					e = [[int(r) for r in t.split()] for t in e.split("|")]
			return (int(n),e)

	rule_text, messages = [x.splitlines() for x in open("Day19-Data.txt").read().split("\n\n")]
	rules = dict(parse_rule(s) for s in rule_text)            
	print("Part 1:", sum(test(m,[0]) for m in messages))       

	rule_text += ["8: 42 | 42 8","11: 42 31 | 42 11 31"]
	rules = dict(parse_rule(s) for s in rule_text)
	print("Part 2:", sum(test(m,[0]) for m in messages)) 
	```

### Work in progress

#### Day 20
