import 'dart:io';

// https://stackoverflow.com/a/57371764/5239250
extension ExtendedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T f(E e, int i)) {
    var i = 0;
    return this.map((e) => f(e, i++));
  }

  void forEachIndexed(void f(E e, int i)) {
    var i = 0;
    this.forEach((e) => f(e, i++));
  }
}

main() {
  final demo = false;
  final path = 'input${demo ? '-demo' : ''}.txt';

  final text_file = File(path)
    .readAsLinesSync();

  {
    final earliest = int.parse(text_file[0]);
    final result = text_file[1]
      .split(',')
      .where((e) => e != 'x')
      .map((e) => int.parse(e))
      .map((id) => [id, id - (earliest % id)])
      .reduce((a, e) => a[1] > e[1] ? e : a)
      .fold(1, (a, e) => a * e);
        
    print('part 1: $result');
  }
  {
    final ids = text_file[1]
      .split(',')
      .map((e) => e == 'x' ? 0 : int.parse(e))
      .mapIndexed((e, i) => [e, i])
      .where((e) => e[0] != 0)
      .toList()
      ;

    var step = ids[0][0];
    var timestamp = step;
    for (var id in ids.sublist(1)) {
      while ((timestamp + id[1]) % id[0] != 0) {
        timestamp += step;
      }
      step *= id[0];
    }
    print('step 2: $timestamp');
  }
}
