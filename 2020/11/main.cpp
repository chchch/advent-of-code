#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <string>
#include <unordered_set>
#include <numeric>
#include <algorithm>

using namespace std::string_literals;

using Seats = std::vector<std::string>;

std::string join_string(const std::vector<std::string>& v, const std::string& s) {
    return std::accumulate(std::next(v.begin()), v.end(), *v.begin(), 
        [&s](const auto& a, const auto& b) {return a + s + b;});
}

const std::vector<std::vector<int>> directions = {
  {-1, -1}, {0, -1}, {1, -1},
  {-1,  0},          {1,  0},
  {-1,  1}, {0,  1}, {1,  1}};

int count_neighbours(const Seats& state, const int i, const int j) {
    int neighbours = 0;
    for (const auto& d: directions) {
        int ii = i + d[0];
        int jj = j + d[1];
        if (ii >= 0 && ii < state.at(0).size()
                && jj >= 0 && jj < state.size()) {
            neighbours += (state.at(jj).at(ii) == '#' ? 1 : 0);
        }
    }
    return neighbours;
}

int count_visibles(const Seats& state, const int i, const int j) {
    int neighbours = 0;
    for (const auto& d: directions) {
        int ii = i + d[0];
        int jj = j + d[1];
        bool found = false;
        while (!found
                && ii >= 0 && ii < state.at(0).size()
                && jj >= 0 && jj < state.size()) {
            if (state.at(jj).at(ii) == '.') {
                ii = ii + d[0];
                jj = jj + d[1];
                continue;
            }
            neighbours += (state.at(jj).at(ii) == '#' ? 1 : 0);
            found = true;
        }
    }
    return neighbours;
}

Seats next_state(Seats state, bool part_1) {
    Seats next;
    std::copy(state.begin(), state.end(), std::back_inserter(next));

    for (int j = 0; j < state.size(); j++) {
       for (int i = 0; i < state[j].size(); i++) {
           const auto& cell = state.at(j).at(i);
           if (cell == '.') {
               continue;
           }
           const int neighbours = part_1 ?
               count_neighbours(state, i, j):
               count_visibles(state, i, j);

           if (cell == 'L') {
               if (neighbours == 0) {
                   next.at(j).at(i) = '#';
               }
           } else {
               if (neighbours >= (part_1 ? 4 : 5)) {
                   next.at(j).at(i) = 'L';
               }
           }
       }
    }
    return next;
}

int main()
{
    Seats seats;

    bool part_1 = true;
    std::string filename = "input"s + (false ? "" : "-demo") + ".txt"s;
    std::ifstream ifs(filename);
    if (ifs) {
        std::copy(std::istream_iterator<std::string>(ifs), 
                std::istream_iterator<std::string>(),
                std::back_inserter(seats));
    } else {
        std::cerr << "Couldn't open " << filename << " for reading\n";
        return 0;
    }

    std::unordered_set<std::string> states{};

    std::string state_string;
    int i = 0;
    while (states.find(state_string = join_string(seats, "")) == states.end()) {
        states.insert(state_string);
        seats = next_state(seats, part_1);
    }

    // std::cout << "" << std::endl;
    // for (const auto& row: seats) {
    //     std::cout << row << std::endl;
    // }

    std::cout << "Occupied: " <<
        std::accumulate(seats.begin(), seats.end(), 0,
            [](const auto& a, const auto& row) {
                 return a + std::count(row.begin(), row.end(), '#');
        }) << std::endl;
}
