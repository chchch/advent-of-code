import 'dart:io';

// https://stackoverflow.com/a/57371764/5239250
extension ExtendedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T f(E e, int i)) {
    var i = 0;
    return this.map((e) => f(e, i++));
  }

  void forEachIndexed(void f(E e, int i)) {
    var i = 0;
    this.forEach((e) => f(e, i++));
  }
}

int sum(int a, int b) {
  return a + b;  
}

clone_addresses(addresses) {
  var next = List<List<String>>();
  for (final row in addresses) {
    var next_row = List<String>();
    for (final cell in row) {
      next_row.add(cell);
    }
    next.add(next_row);
  }
  return next;
}

main() {
  final demo = false;
  final part_2 = true;
  final path = 'input${demo ? '-demo' : ''}${demo && part_2 ? '-02' : ''}.txt';

  final maskBit = {
    '0': (c) => '0',
    '1': (c) => '1',
    'X': (c) => c,
  };

  var mask = List<String>();
  var values = Map<int, int>();

  for (final line in File(path).readAsLinesSync()) {
    final fields = line.split(' = ');
    if (fields[0] == 'mask') {
      mask = fields[1].split('');
    } else {
      final address = int
        .parse(fields[0].substring(4, fields[0].length - 1));

      values[address] = int.parse(int
          .parse(fields[1])
          .toRadixString(2)
          .padLeft(36, '0')
          .split('')
          .mapIndexed((e, i) => maskBit[mask[i]](e))
          .join(''),
        radix: 2);
    }
  }

  print('part 1: ${values.values.reduce(sum)}');

  mask.clear();
  values.clear();

  final Map<String, String Function(String)> maskAddressBit = {
    '0': (c) => c,
    '1': (c) => '1',
    'X': (c) => 'X',
  };

  for (final line in File(path).readAsLinesSync()) {
    final fields = line.split(' = ');
    if (fields[0] == 'mask') {
      mask = fields[1].split('');
    } else {
      final address = int
        .parse(fields[0].substring(4, fields[0].length - 1))
        .toRadixString(2)
        .padLeft(36, '0')
        .split('');

      final address_masked = address
        .mapIndexed((c, i) => maskAddressBit[mask[i]](c))
        .toList();

      var addresses = List<List<String>>.from([address_masked]);
      address_masked.forEachIndexed((c, i) {
        if (c == 'X') {
          var new_addresses = List<List<String>>();
          for (var address in addresses) {
            address[i] = '1';
            new_addresses.add([...address]);
            address[i] = '0';
            new_addresses.add([...address]);
          }
          addresses.clear();
          addresses.addAll(new_addresses);
          
        }
      });

      final value = int.parse(fields[1]);
      addresses
        .map((e) => int.parse(e.join(''), radix: 2))
        .forEach((e) => values[e] = value);

    }
  }
  print('part 2: ${values.values.reduce(sum)}');
}
