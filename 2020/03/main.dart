import 'dart:async';
import 'dart:io';
import 'dart:convert';

class Slope {
  final int dx, dy;
  var x = 0, y = 0, n = 0;

  Slope(this.dx, this.dy);
}

void main() async {
  // final part_1 = true;
  final path = true ? 'input.txt': 'input-demo.txt';

  final map = File(path)
    .readAsLinesSync();

  var slope = [Slope(1, 1), Slope(3, 1), Slope(5, 1),
    Slope(7, 1), Slope(1, 2)];

  final n = map.length;
  final m = map[0].length;

  for (var i = 1; i < n; i++) {
    slope
      .map((v) {
        v.x = (v.x + v.dx) % m;
        v.y = v.y + v.dy;
        if (v.y < n && map[v.y][v.x] == '#') {
            v.n++;
        }
        return v;
      })
      .toList();
  }

  // print(slope);

  final n_trees = slope
    .fold(1, (a, e) => a * e.n);

  print(n_trees);
}
